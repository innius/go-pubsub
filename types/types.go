package types

import (
	"context"
	"encoding/json"
)

const (
	MessageSourceAttr = "source"
	MessageTypeAttr   = "event"
)

type EventMessage interface {
	EventType() string
	EventSource() string
}

type RawEvent struct {
	EventType   string          `json:"event_type,omitempty"`
	EventSource string          `json:"event_source,omitempty"`
	RawMessage  json.RawMessage `json:"Message"`
}

type EventHandler interface {
	EventHandler(c context.Context, e RawEvent) error
}


// Handlers is a collection of handlers
type Handlers []EventHandler

func (hs Handlers) EventHandler(c context.Context, e RawEvent) error {
	for _, h := range hs {
		if err := h.EventHandler(c, e); err != nil {
			return err
		}
	}
	return nil
}


module bitbucket.org/innius/go-pubsub

go 1.15

require (
	github.com/aws/aws-lambda-go v1.8.1
	github.com/aws/aws-sdk-go v1.19.19
	github.com/gopherjs/gopherjs v0.0.0-20190328170749-bb2674552d8f // indirect
	github.com/jtolds/gls v4.20.0+incompatible // indirect
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.4.0
	github.com/smartystreets/assertions v0.0.0-20190116191733-b6c0e53d7304 // indirect
	github.com/smartystreets/goconvey v0.0.0-20190222223459-a17d461953aa
	github.com/stretchr/testify v1.3.0
	golang.org/x/net v0.0.0-20190327214358-63eda1eb0650 // indirect
	golang.org/x/sys v0.0.0-20190222072716-a9d3bda3a223 // indirect
	golang.org/x/text v0.3.1-0.20180807135948-17ff2d5776d2 // indirect
)

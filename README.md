# go-pubsub

`go-pubsub` is a publisher/subscriber package for the innius services.

## Background

In the past years several innius (formerly to-increase) pub/sub packages have been created.
Most of them were built in the early innius days and are not in line with our current requirements.
A list of known packages:

* `go-sns`
* `go-event-queue`

Currently, there are two Simple Notification Service (SNS) topics for innius domain events:

* Legacy `chair-machine-service-EventTopic-1ST3S2LFK4LRR` (https)
   * `KPI serverless APIs (OEE, EE, CU, breakdown)`
   * `Connection service`
   * `sensor-service`
   * `location-service-v2` (pending)
   * `messaging-service-v2`
* `chair-domain-events` (Lambda)
   * `kpi-service`
   * `usage-service`
   * `invoice-service`
   * `logistics-service`
   * `demomachine-service`

This package aims to replaces the old pub/sub packages and provide a better API for
innius pub/sub use cases.
Implementation should be decoupled from the interfaces.
This package comprises the following areas:

* Events (all event messages are registered here):
   * Machines
   * Companies
   * Persons
   * Sensors
   * KPIs
   * Connections
* Subscriptions:
   * SQS
   * Lambda

## Used by

Innius services that make use of `go-pubsub`:

* [apikey-service](https://bitbucket.org/innius/apikey-service/src/master/)
* [machine-service](https://bitbucket.org/to-increase/machine-service/src/master/)
* [sensor-service](https://bitbucket.org/to-increase/sensor-service/src/master)
* [monitoring-service-v3](https://bitbucket.org/innius/monitoring-service-v3/src/master)
* [messaging-service-v2](https://bitbucket.org/to-increase/messaging-service-v2/src/master)
* [collateral-service-v2](https://bitbucket.org/innius/collateral-service-v2/src/master/)
* [kpi-service](https://bitbucket.org/to-increase/kpi-service/src/master/)
* [domainevents-slack-lambda-v2](https://bitbucket.org/innius/domainevents-slack-lambda-v2/src/master/)

## Usage

### Publishing messages

To publish messages, use:

```go
pub := NewPublisher(snsClient)
pub.Publish(ctx, machineevents.MachineCreatedEvent{})
```

### Subscribing to messages

To subscribe to messages, use one of the following: AWS SQS or AWS Lambda.

#### Using AWS SQS

Using AWS Simple Queue Service (SQS):

```go
sub := sqsubscription.NewSubscriber(sqs.New(session.Must(session.NewSession(cfg))), handler, Config{
    QueueUrl: "http://sqs:9324/queue/queue1",    
})

go func() {
	err := sub.Run(ctx)
	if err != nil {
		log.Fatal(err) // processing has been terminated
	}
}
```

#### Using AWS Lambda

Using AWS Lambda:

```go
lambda.Start(lambdasubscriber.NewSubscriber(handler))
```

## Testing

Start the Docker `sns` and `sqs` services, run the tests,
and inspect the coverage report with the following commands:
```
docker-compose up -d
go test -coverprofile=coverage.out -v ./...
go tool cover -html=coverage.out
docker-compose down
```

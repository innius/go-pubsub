package go_pubsub

import (
	"bitbucket.org/innius/go-pubsub/events/machines"
	"context"
	"encoding/json"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/aws/aws-sdk-go/service/sns/snsiface"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

type snsMock struct {
	snsiface.SNSAPI

	messages []*sns.PublishInput
}

func (m *snsMock) PublishWithContext(c aws.Context, in *sns.PublishInput, opts ...request.Option) (*sns.PublishOutput, error) {
	if m.messages != nil {
		m.messages = append(m.messages, in)
	}
	return nil, nil
}

func TestPublisher(t *testing.T) {
	Convey("Publisher", t, func() {
		ctx := context.Background()
		pub := NewPublisher(&snsMock{}, "foo", false)
		So(pub.Publish(ctx, machineevents.MachineCreatedEvent{}), ShouldBeNil)
		Convey("Legacy messages", func() {
			m := &snsMock{messages: []*sns.PublishInput{}}
			pub := NewPublisher(m, "foo", true)
			//Convey("Company events", func() {
			//	So(pub.Publish(ctx, companyevents.CompanyCreatedEvent{}), ShouldBeNil)
			//	So(m.messages, ShouldHaveLength, 2)
			//})
			Convey("Machine events", func() {
				So(pub.Publish(ctx, machineevents.MachineCreatedEvent{}), ShouldBeNil)
				So(m.messages, ShouldHaveLength, 2)

				bits, err := json.Marshal(machineevents.MachineCreatedEvent{})
				So(err, ShouldBeNil)

				So(*m.messages[0].Message, ShouldEqual, *aws.String(string(bits)))
				So(*m.messages[1].Message, ShouldEqual, *aws.String(string(bits)))
			})

		})
	})
}

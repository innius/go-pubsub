package sensorevents

import (
	"context"
	"encoding/json"

	"bitbucket.org/innius/go-pubsub/types"
	"github.com/sirupsen/logrus"
)

type SensorEventHandler struct {
	onSensorCreatedEventHandler                     func(context.Context, SensorCreatedEvent) error
	onSensorDeletedEventHandler                     func(context.Context, SensorDeletedEvent) error
	onSensorRateChangedEventHandler                 func(context.Context, SensorRateChangedEvent) error
	onSensorKindChangedEventHandler                 func(context.Context, SensorKindChangedEvent) error
	onSensorBatchChangedEventHandler                func(context.Context, SensorBatchChangedEvent) error
	onSensorBatchAggregationTypeChangedEventHandler func(context.Context, SensorBatchAggregationTypeChangedEvent) error
	onSensorDataTypeChangedEventHandler             func(context.Context, SensorDataTypeChangedEvent) error
	onSensorDescriptionChangedEventHandler          func(context.Context, SensorDescriptionChangedEvent) error
	onSensorNameChangedEventHandler                 func(ctx context.Context, event SensorNameChangedEvent) error
	onSensorValueMappingChangedEventHandler         func(ctx context.Context, event SensorValueMappingChangedEvent) error
	onSensorPhysicalIdChangedEventHandler           func(ctx context.Context, event SensorPhysicalIdChangedEvent) error
}

func (m *SensorEventHandler) EventHandler(c context.Context, e types.RawEvent) error {
	if e.EventSource != source {
		return nil
	}

	switch e.EventType {
	case sensorCreatedEventName:
		return m.onSensorCreated(c, e)
	case sensorDeletedEventName:
		return m.onSensorDeleted(c, e)
	case sensorRateChangedEventName:
		return m.onSensorRateChanged(c, e)
	case sensorKindChangedEventName:
		return m.onSensorKindChanged(c, e)
	case sensorBatchChangedEventName:
		return m.onSensorBatchChanged(c, e)
	case sensorBatchAggregationTypeChangedEventName:
		return m.onSensorBatchAggregationTypeChanged(c, e)
	case sensorDataTypeChangedEventName:
		return m.onSensorDataTypeChanged(c, e)
	case sensorDescriptionChangedEventName:
		return m.onSensorDescriptionChanged(c, e)
	case sensorNameChangedEventName:
		return m.onSensorNameChanged(c, e)
	case sensorValueMappingChangedEventName:
		return m.onSensorValueMappingChanged(c, e)
	case sensorPhysicalIdChangedEventName:
		return m.onSensorPhysicalIdChanged(c, e)
	default:
		logrus.Warnf("unsupported event type %v", e.EventType)
	}
	return nil
}

func (m *SensorEventHandler) OnSensorCreated(h func(c context.Context, h SensorCreatedEvent) error) {
	m.onSensorCreatedEventHandler = h
}

func (m *SensorEventHandler) onSensorCreated(c context.Context, e types.RawEvent) error {
	if m.onSensorCreatedEventHandler == nil {
		return nil
	}
	event := SensorCreatedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onSensorCreatedEventHandler(c, event)
}

func (m *SensorEventHandler) OnSensorDeleted(h func(c context.Context, h SensorDeletedEvent) error) {
	m.onSensorDeletedEventHandler = h
}

func (m *SensorEventHandler) onSensorDeleted(c context.Context, e types.RawEvent) error {
	if m.onSensorDeletedEventHandler == nil {
		return nil
	}
	event := SensorDeletedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onSensorDeletedEventHandler(c, event)
}

func (m *SensorEventHandler) OnSensorRateChanged(h func(c context.Context, h SensorRateChangedEvent) error) {
	m.onSensorRateChangedEventHandler = h
}

func (m *SensorEventHandler) onSensorRateChanged(c context.Context, e types.RawEvent) error {
	if m.onSensorRateChangedEventHandler == nil {
		return nil
	}
	event := SensorRateChangedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onSensorRateChangedEventHandler(c, event)
}

func (m *SensorEventHandler) OnSensorKindChanged(h func(c context.Context, h SensorKindChangedEvent) error) {
	m.onSensorKindChangedEventHandler = h
}

func (m *SensorEventHandler) onSensorKindChanged(c context.Context, e types.RawEvent) error {
	if m.onSensorKindChangedEventHandler == nil {
		return nil
	}
	event := SensorKindChangedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onSensorKindChangedEventHandler(c, event)
}

func (m *SensorEventHandler) OnSensorBatchChanged(h func(c context.Context, h SensorBatchChangedEvent) error) {
	m.onSensorBatchChangedEventHandler = h
}

func (m *SensorEventHandler) onSensorBatchChanged(c context.Context, e types.RawEvent) error {
	if m.onSensorBatchChangedEventHandler == nil {
		return nil
	}
	event := SensorBatchChangedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onSensorBatchChangedEventHandler(c, event)
}

func (m *SensorEventHandler) OnSensorBatchAggregationTypeChanged(h func(c context.Context, h SensorBatchAggregationTypeChangedEvent) error) {
	m.onSensorBatchAggregationTypeChangedEventHandler = h
}

func (m *SensorEventHandler) onSensorBatchAggregationTypeChanged(c context.Context, e types.RawEvent) error {
	if m.onSensorBatchAggregationTypeChangedEventHandler == nil {
		return nil
	}
	event := SensorBatchAggregationTypeChangedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onSensorBatchAggregationTypeChangedEventHandler(c, event)
}

func (m *SensorEventHandler) OnSensorDataTypeChanged(h func(c context.Context, h SensorDataTypeChangedEvent) error) {
	m.onSensorDataTypeChangedEventHandler = h
}

func (m *SensorEventHandler) onSensorDataTypeChanged(c context.Context, e types.RawEvent) error {
	if m.onSensorDataTypeChangedEventHandler == nil {
		return nil
	}
	event := SensorDataTypeChangedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onSensorDataTypeChangedEventHandler(c, event)
}

func (m *SensorEventHandler) OnSensorDescriptionChanged(h func(c context.Context, h SensorDescriptionChangedEvent) error) {
	m.onSensorDescriptionChangedEventHandler = h
}

func (m *SensorEventHandler) onSensorDescriptionChanged(c context.Context, e types.RawEvent) error {
	if m.onSensorDescriptionChangedEventHandler == nil {
		return nil
	}
	event := SensorDescriptionChangedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onSensorDescriptionChangedEventHandler(c, event)
}

func (m *SensorEventHandler) OnSensorNameChanged(h func(c context.Context, h SensorNameChangedEvent) error) {
	m.onSensorNameChangedEventHandler = h
}

func (m *SensorEventHandler) onSensorNameChanged(c context.Context, e types.RawEvent) error {
	if m.onSensorNameChangedEventHandler == nil {
		return nil
	}
	event := SensorNameChangedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onSensorNameChangedEventHandler(c, event)
}

func (m *SensorEventHandler) OnSensorValueMappingChanged(h func(c context.Context, h SensorValueMappingChangedEvent) error) {
	m.onSensorValueMappingChangedEventHandler = h
}

func (m *SensorEventHandler) onSensorValueMappingChanged(c context.Context, e types.RawEvent) error {
	if m.onSensorValueMappingChangedEventHandler == nil {
		return nil
	}
	event := SensorValueMappingChangedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onSensorValueMappingChangedEventHandler(c, event)
}

func (m *SensorEventHandler) OnSensorPhysicalIdChanged(h func(c context.Context, e SensorPhysicalIdChangedEvent) error) {
	m.onSensorPhysicalIdChangedEventHandler = h
}

func (m *SensorEventHandler) onSensorPhysicalIdChanged(c context.Context, e types.RawEvent) error {
	if m.onSensorPhysicalIdChangedEventHandler == nil {
		return nil
	}
	event := SensorPhysicalIdChangedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onSensorPhysicalIdChangedEventHandler(c, event)
}

package sensorevents

import (
	"context"
	"encoding/json"
	"testing"

	"bitbucket.org/innius/go-pubsub/types"
	"github.com/stretchr/testify/assert"
)

func TestSubscriptions(t *testing.T) {
	tests := []types.EventMessage{
		SensorCreatedEvent{},
		SensorDeletedEvent{},
		SensorKindChangedEvent{},
		SensorDataTypeChangedEvent{},
		SensorBatchAggregationTypeChangedEvent{},
		SensorRateChangedEvent{},
		SensorDescriptionChangedEvent{},
		SensorValueMappingChangedEvent{},
	}

	for _, evt := range tests {
		s := &SensorEventHandler{}
		src := ""

		setSource := func(eventType string) error {
			src = eventType
			return nil
		}

		s.OnSensorCreated(func(c context.Context, e SensorCreatedEvent) error {
			return setSource(e.EventType())
		})
		s.OnSensorDeleted(func(c context.Context, e SensorDeletedEvent) error {
			return setSource(e.EventType())
		})
		s.OnSensorBatchChanged(func(c context.Context, e SensorBatchChangedEvent) error {
			return setSource(e.EventType())
		})
		s.OnSensorBatchAggregationTypeChanged(func(c context.Context, e SensorBatchAggregationTypeChangedEvent) error {
			return setSource(e.EventType())
		})
		s.OnSensorKindChanged(func(c context.Context, e SensorKindChangedEvent) error {
			return setSource(e.EventType())
		})
		s.OnSensorDataTypeChanged(func(c context.Context, e SensorDataTypeChangedEvent) error {
			return setSource(e.EventType())
		})
		s.OnSensorRateChanged(func(c context.Context, e SensorRateChangedEvent) error {
			return setSource(e.EventType())
		})
		s.OnSensorDescriptionChanged(func(c context.Context, e SensorDescriptionChangedEvent) error {
			return setSource(e.EventType())
		})
		s.OnSensorNameChanged(func(c context.Context, e SensorNameChangedEvent) error {
			src = e.EventType()
			return nil
		})
		s.OnSensorValueMappingChanged(func(c context.Context, e SensorValueMappingChangedEvent) error {
			src = e.EventType()
			return nil
		})
		s.OnSensorPhysicalIdChanged(func(c context.Context, e SensorPhysicalIdChangedEvent) error {
			src = e.EventType()
			return nil
		})

		bits, err := json.Marshal(evt)
		assert.NoError(t, err)
		raw := types.RawEvent{
			EventType:   evt.EventType(),
			EventSource: evt.EventSource(),
			RawMessage:  bits,
		}
		assert.NoError(t, s.EventHandler(context.Background(), raw))
		assert.Equal(t, evt.EventType(), src)
	}
}

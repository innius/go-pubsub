package sensorevents

//TODO decide whether batchSensorConfigs need their own events for sensorCreation

const source = "sensors"

const (
	sensorCreatedEventName                     = "SensorCreatedEvent"
	sensorDeletedEventName                     = "SensorDeletedEvent"
	sensorKindChangedEventName                 = "SensorKindChangedEvent"
	sensorRateChangedEventName                 = "SensorRateChangedEvent"
	sensorBatchChangedEventName                = "SensorBatchChangedEvent"
	sensorBatchAggregationTypeChangedEventName = "SensorBatchAggregationTypeChangedEvent"
	sensorDataTypeChangedEventName             = "SensorDataTypeChangedEvent"
	sensorDescriptionChangedEventName          = "SensorDescriptionChangedEvent"
	sensorNameChangedEventName                 = "SensorNameChangedEvent"
	sensorValueMappingChangedEventName         = "SensorValueMappingChangedEvent"
	sensorPhysicalIdChangedEventName           = "SensorPhysicalIdChangedEvent"
)

// SensorCreatedEvent is sent when a new sensor is configured
type SensorCreatedEvent struct {
	CompanyID       string               `json:"company_id"`
	MachineID       string               `json:"machine_id"`
	SensorID        string               `json:"sensor_id"`
	PhysicalID      string               `json:"physical_id"`
	Name            string               `json:"name"`
	BatchSensor     string               `json:"batch_sensor"`
	AggregationType AggregationType      `json:"aggregation_type"`
	SensorKind      SensorKind           `json:"sensor_kind"`
	SensorDataType  SensorDataType       `json:"sensor_data_type"`
	Rate            int                  `json:"rate"`
	ValueMapping    []SensorValueMapping `json:"value_mapping,omitempty"`
	Description     string               `json:"description,omitempty"`
}

func (e SensorCreatedEvent) EventType() string {
	return sensorCreatedEventName
}

func (e SensorCreatedEvent) EventSource() string {
	return source
}

// SensorDeletedEvent is sent when a sensor is deleted
type SensorDeletedEvent struct {
	CompanyID      string         `json:"company_id"`
	MachineID      string         `json:"machine_id"`
	SensorID       string         `json:"sensor_id"`
	SensorDataType SensorDataType `json:"sensor_data_type"`
	Rate           int            `json:"rate"`
}

func (e SensorDeletedEvent) EventType() string {
	return sensorDeletedEventName
}

func (e SensorDeletedEvent) EventSource() string {
	return source
}

type SensorKindChangedEvent struct {
	CompanyID      string         `json:"company_id"`
	MachineID      string         `json:"machine_id"`
	SensorID       string         `json:"sensor_id"`
	SensorDataType SensorDataType `json:"sensor_data_type"`
	NewKind        SensorKind     `json:"new_kind"`
}

func (e SensorKindChangedEvent) EventType() string {
	return sensorKindChangedEventName
}

func (e SensorKindChangedEvent) EventSource() string {
	return source
}

type SensorRateChangedEvent struct {
	CompanyID      string         `json:"company_id"`
	MachineID      string         `json:"machine_id"`
	SensorID       string         `json:"sensor_id"`
	SensorDataType SensorDataType `json:"sensor_data_type"`
	NewValue       int            `json:"new_value"`
}

func (e SensorRateChangedEvent) EventType() string {
	return sensorRateChangedEventName
}

func (e SensorRateChangedEvent) EventSource() string {
	return source
}

type SensorBatchChangedEvent struct {
	CompanyID      string         `json:"company_id"`
	MachineID      string         `json:"machine_id"`
	SensorID       string         `json:"sensor_id"`
	SensorDataType SensorDataType `json:"sensor_data_type"`
	NewBatchSensor string         `json:"new_batch_sensor"`
}

func (e SensorBatchChangedEvent) EventType() string {
	return sensorBatchChangedEventName
}

func (e SensorBatchChangedEvent) EventSource() string {
	return source
}

type SensorBatchAggregationTypeChangedEvent struct {
	CompanyID          string          `json:"company_id"`
	MachineID          string          `json:"machine_id"`
	SensorID           string          `json:"sensor_id"`
	NewAggregationType AggregationType `json:"new_aggregation_type"`
}

func (e SensorBatchAggregationTypeChangedEvent) EventType() string {
	return sensorBatchAggregationTypeChangedEventName
}

func (e SensorBatchAggregationTypeChangedEvent) EventSource() string {
	return source
}

type SensorDataTypeChangedEvent struct {
	CompanyID string         `json:"company_id"`
	MachineID string         `json:"machine_id"`
	SensorID  string         `json:"sensor_id"`
	NewType   SensorDataType `json:"new_type"`
}

func (e SensorDataTypeChangedEvent) EventType() string {
	return sensorDataTypeChangedEventName
}

func (e SensorDataTypeChangedEvent) EventSource() string {
	return source
}

type SensorDescriptionChangedEvent struct {
	CompanyID      string `json:"company_id"`
	MachineID      string `json:"machine_id"`
	SensorID       string `json:"sensor_id"`
	NewDescription string `json:"new_description"`
}

func (e SensorDescriptionChangedEvent) EventType() string {
	return sensorDescriptionChangedEventName
}

func (e SensorDescriptionChangedEvent) EventSource() string {
	return source
}

type SensorNameChangedEvent struct {
	CompanyID string `json:"company_id"`
	MachineID string `json:"machine_id"`
	SensorID  string `json:"sensor_id"`
	NewName   string `json:"new_name"`
}

func (e SensorNameChangedEvent) EventType() string {
	return sensorNameChangedEventName
}

func (e SensorNameChangedEvent) EventSource() string {
	return source
}

type SensorValueMapping struct {
	Value       string `json:"value"`
	Description string `json:"description"`
}

type SensorValueMappingChangedEvent struct {
	CompanyID    string               `json:"company_id"`
	MachineID    string               `json:"machine_id"`
	SensorID     string               `json:"sensor_id"`
	ValueMapping []SensorValueMapping `json:"value_mapping,omitempty"`
}

func (e SensorValueMappingChangedEvent) EventType() string {
	return sensorValueMappingChangedEventName
}

func (e SensorValueMappingChangedEvent) EventSource() string {
	return source
}

type SensorPhysicalIdChangedEvent struct {
	CompanyID string `json:"company_id"`
	MachineID string `json:"machine_id"`
	SensorID  string `json:"sensor_id"`
	NewID     string `json:"new_id"`
}

func (e SensorPhysicalIdChangedEvent) EventType() string {
	return sensorPhysicalIdChangedEventName
}

func (e SensorPhysicalIdChangedEvent) EventSource() string {
	return source
}

type SensorKind int

type SensorDataType string

type AggregationType int

const (
	Physical SensorKind = iota
	Virtual
	Manual
	Scheduled

	Discrete   SensorDataType = "discrete"
	Continuous SensorDataType = "continuous"
	GPS        SensorDataType = "gps"
)

const (
	Unknown AggregationType = iota
	Average
	Last
	Sum
)

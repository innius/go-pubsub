package kpievents

import "bitbucket.org/innius/go-pubsub/messaging"

const source = "kpi"

const (
	kpiThresholdCrossedEventName = "kpiThresholdCrossed"
	KpiConfigCreatedEventName    = "kpiConfigCreated"
	KpiConfigDeletedEventName    = "kpiConfigDeleted"
)

// KpiThresholdCrossedEvent is sent when a kpi threshold is crossed.
type KpiThresholdCrossedEvent struct {
	Machinetrn string                  `json:"machinetrn"`
	Kpi        string                  `json:"kpi"`
	Status     messaging.MessageStatus `json:"status"`
	Value      float64                 `json:"value"`
	Timestamp  int64                   `json:"timestamp"`
}

func (e KpiThresholdCrossedEvent) EventType() string {
	return kpiThresholdCrossedEventName
}

func (e KpiThresholdCrossedEvent) EventSource() string {
	return source
}

type KpiConfigCreatedEvent struct {
	Machinetrn string `json:"machinetrn"`
	Kpi        string `json:"kpi"`
}

func (e KpiConfigCreatedEvent) EventType() string {
	return KpiConfigCreatedEventName
}

func (e KpiConfigCreatedEvent) EventSource() string {
	return source
}

type KpiConfigDeletedEvent struct {
	Machinetrn string `json:"machinetrn"`
	Kpi        string `json:"kpi"`
}

func (e KpiConfigDeletedEvent) EventType() string {
	return KpiConfigDeletedEventName
}

func (e KpiConfigDeletedEvent) EventSource() string {
	return source
}
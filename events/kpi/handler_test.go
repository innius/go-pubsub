package kpievents

import (
	"context"
	"encoding/json"
	"testing"

	"bitbucket.org/innius/go-pubsub/types"
	"github.com/stretchr/testify/assert"
)

func TestSubscriptions(t *testing.T) {
	tests := []types.EventMessage{
		KpiThresholdCrossedEvent{},
		KpiConfigCreatedEvent{},
		KpiConfigDeletedEvent{},
	}

	for _, evt := range tests {
		s := &KpiEventHandler{}
		src := ""
		setSrcFunc := func(eventType string) error {
			src = eventType
			return nil
		}
		s.OnKpiThresholdCrossed(func(c context.Context, e KpiThresholdCrossedEvent) error {
			return setSrcFunc(e.EventType())
		})
		s.OnKpiConfigCreated(func (c context.Context, e KpiConfigCreatedEvent) error {
			return setSrcFunc(e.EventType())
		})
		s.OnKpiConfigDeleted(func(c context.Context, e KpiConfigDeletedEvent) error {
			return setSrcFunc(e.EventType())
		})

		bits, err := json.Marshal(evt)
		assert.NoError(t, err)
		raw := types.RawEvent{
			EventType:   evt.EventType(),
			EventSource: evt.EventSource(),
			RawMessage:  bits,
		}
		assert.NoError(t, s.EventHandler(context.Background(), raw))
		assert.Equal(t, evt.EventType(), src)
	}
}

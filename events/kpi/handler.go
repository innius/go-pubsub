package kpievents

import (
	"context"
	"encoding/json"

	"bitbucket.org/innius/go-pubsub/types"
	"github.com/sirupsen/logrus"
)

type KpiEventHandler struct {
	onKpiThresholdCrossedHandler func(context.Context, KpiThresholdCrossedEvent) error
	onKpiConfigCreatedHandler    func(context.Context, KpiConfigCreatedEvent) error
	onKpiConfigDeletedHandler    func(context.Context, KpiConfigDeletedEvent) error
}

func (m *KpiEventHandler) EventHandler(c context.Context, e types.RawEvent) error {
	if e.EventSource != source {
		return nil
	}

	switch e.EventType {
	case kpiThresholdCrossedEventName:
		return m.onKpiThresholdCrossed(c, e)
	case KpiConfigCreatedEventName:
		return m.onKpiConfigCreated(c, e)
	case KpiConfigDeletedEventName:
		return m.onKpiConfigDeleted(c, e)
	default:
		logrus.Warnf("unsupported event type %v", e.EventType)
		return nil
	}
}

// OnSensorForecastThresholdCrossed registers a handler for KpiThresholdCrossedEvent
func (m *KpiEventHandler) OnKpiThresholdCrossed(h func(c context.Context, h KpiThresholdCrossedEvent) error) {
	m.onKpiThresholdCrossedHandler = h
}

func (m *KpiEventHandler) onKpiThresholdCrossed(c context.Context, e types.RawEvent) error {
	if m.onKpiThresholdCrossedHandler == nil {
		return nil
	}
	event := KpiThresholdCrossedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onKpiThresholdCrossedHandler(c, event)
}

func (m *KpiEventHandler) OnKpiConfigCreated(h func(c context.Context, h KpiConfigCreatedEvent) error) {
	m.onKpiConfigCreatedHandler = h
}

func (m *KpiEventHandler) onKpiConfigCreated(c context.Context, e types.RawEvent) error {
	if m.onKpiConfigCreatedHandler == nil {
		return nil
	}
	event := KpiConfigCreatedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onKpiConfigCreatedHandler(c, event)
}

func (m *KpiEventHandler) OnKpiConfigDeleted(h func(c context.Context, h KpiConfigDeletedEvent) error) {
	m.onKpiConfigDeletedHandler = h
}

func (m *KpiEventHandler) onKpiConfigDeleted(c context.Context, e types.RawEvent) error {
	if m.onKpiConfigDeletedHandler == nil {
		return nil
	}
	event := KpiConfigDeletedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onKpiConfigDeletedHandler(c, event)
}

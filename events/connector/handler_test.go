package connector

import (
	"context"
	"encoding/json"
	"testing"

	"bitbucket.org/innius/go-pubsub/types"
	"github.com/stretchr/testify/assert"
)

func TestSubscriptions(t *testing.T) {
	tests := []types.EventMessage{
		MachineAttachedEvent{},
		MachineDetachedEvent{},
	}

	for _, evt := range tests {
		s := &ConnectorEventHandler{}
		src := ""
		s.OnMachineAttached(func(c context.Context, e MachineAttachedEvent) error {
			src = e.EventType()
			return nil
		})
		s.OnMachineDetached(func(c context.Context, e MachineDetachedEvent) error {
			src = e.EventType()
			return nil
		})

		bits, err := json.Marshal(evt)
		assert.NoError(t, err)
		raw := types.RawEvent{
			EventType:   evt.EventType(),
			EventSource: evt.EventSource(),
			RawMessage:  bits,
		}
		assert.NoError(t, s.EventHandler(context.Background(), raw))
		assert.Equal(t, evt.EventType(), src)
	}
}

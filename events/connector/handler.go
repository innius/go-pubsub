package connector

import (
	"context"
	"encoding/json"

	"bitbucket.org/innius/go-pubsub/types"
	"github.com/sirupsen/logrus"
)

// The ConnectorEventHandler is responsible for managing all events associated with the innius connector.
// It's important to note that this is different from a "connection," which refers to a feature of a machine or sensor.
type ConnectorEventHandler struct {
	onMachineAttachedHandler func(context.Context, MachineAttachedEvent) error
	onMachineDetachedHandler func(context.Context, MachineDetachedEvent) error
}

func (m *ConnectorEventHandler) EventHandler(c context.Context, e types.RawEvent) error {
	if e.EventSource != source {
		return nil
	}

	switch e.EventType {
	case machineAttachedEventName:
		return m.onMachineAttached(c, e)
	case machineDetachedEventName:
		return m.onMachineDetached(c, e)
	default:
		logrus.Warnf("unsupported event type %v", e.EventType)
		return nil
	}
}

// MachineAttachedEvent is fired when a machine is associated with the connector
func (m *ConnectorEventHandler) OnMachineAttached(h func(c context.Context, h MachineAttachedEvent) error) {
	m.onMachineAttachedHandler = h
}

func (m *ConnectorEventHandler) onMachineAttached(c context.Context, e types.RawEvent) error {
	if m.onMachineAttachedHandler == nil {
		return nil
	}
	event := MachineAttachedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onMachineAttachedHandler(c, event)
}

// MachineDetachedEvent is fired when a machine is dissociated with the connector
func (m *ConnectorEventHandler) OnMachineDetached(h func(c context.Context, h MachineDetachedEvent) error) {
	m.onMachineDetachedHandler = h
}

func (m *ConnectorEventHandler) onMachineDetached(c context.Context, e types.RawEvent) error {
	if m.onMachineDetachedHandler == nil {
		return nil
	}
	event := MachineDetachedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onMachineDetachedHandler(c, event)
}

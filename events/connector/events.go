package connector

const source = "connector"

const (
	machineAttachedEventName = "MachineAttached"
	machineDetachedEventName = "MachineDetached"
)

// MachineConnectedEvent is sent when a machine is associated with a connector
type MachineAttachedEvent struct {
	MachineID   string `json:"machine_trn,omitempty"`
	ConnectorID string `json:"connector_id,omitempty"`
	// The DeviceID refers to the unique identifier of the physical device, which in our situation
	// is an AWS IoT Thing. This ID usually matches the ConnectorID.
	// However, there are instances where some connectors have a different device ID.
	DeviceID  string `json:"device_id,omitempty"`
	Timestamp int64  `json:"timestamp,omitempty"`
}

func (e MachineAttachedEvent) EventType() string {
	return machineAttachedEventName
}

func (e MachineAttachedEvent) EventSource() string {
	return source
}

// MachineConnectedEvent is sent when a machine is dissociated from a connector
type MachineDetachedEvent struct {
	MachineID   string `json:"machine_trn,omitempty"`
	ConnectorID string `json:"connector_id,omitempty"`
	// The DeviceID refers to the unique identifier of the physical device, which in our situation
	// is an AWS IoT Thing. This ID usually matches the ConnectorID.
	// However, there are instances where some connectors have a different device ID.
	DeviceID  string `json:"device_id,omitempty"`
	Timestamp int64  `json:"timestamp,omitempty"`
}

func (e MachineDetachedEvent) EventType() string {
	return machineDetachedEventName
}

func (e MachineDetachedEvent) EventSource() string {
	return source
}

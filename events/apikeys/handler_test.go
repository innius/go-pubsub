package apikeyevents

import (
	"context"
	"encoding/json"
	"testing"

	"bitbucket.org/innius/go-pubsub/types"
	"github.com/stretchr/testify/assert"
)

func TestSubscriptions(t *testing.T) {
	tests := []types.EventMessage{
		ApiKeyCreatedEvent{},
		ApiKeyCreatedEvent{},
	}

	for _, evt := range tests {
		s := &ApiKeyEventHandler{}
		src := ""
		s.OnApiKeyCreated(func(c context.Context, e ApiKeyCreatedEvent) error {
			src = e.EventType()
			return nil
		})
		s.OnApiKeyDeleted(func(c context.Context, e ApiKeyDeletedEvent) error {
			src = e.EventType()
			return nil
		})

		bits, err := json.Marshal(evt)
		assert.NoError(t, err)
		raw := types.RawEvent{
			EventType:   evt.EventType(),
			EventSource: evt.EventSource(),
			RawMessage:  bits,
		}
		assert.NoError(t, s.EventHandler(context.Background(), raw))
		assert.Equal(t, evt.EventType(), src)
	}
}

package apikeyevents

import (
	"context"
	"encoding/json"

	"bitbucket.org/innius/go-pubsub/types"
	"github.com/sirupsen/logrus"
)

type ApiKeyEventHandler struct {
	onApiKeyCreatedEventHandler func(context.Context, ApiKeyCreatedEvent) error
	onApiKeyDeletedEventHandler func(context.Context, ApiKeyDeletedEvent) error
}

func (m *ApiKeyEventHandler) EventHandler(c context.Context, e types.RawEvent) error {
	if e.EventSource != source {
		return nil
	}

	switch e.EventType {
	case apiKeyCreatedEventName:
		return m.onApiKeyCreated(c, e)
	case apiKeyDeletedEventName:
		return m.onApiKeyDeleted(c, e)
	default:
		logrus.Warnf("unsupported event type %v", e.EventType)
	}
	return nil
}

func (m *ApiKeyEventHandler) OnApiKeyCreated(h func(c context.Context, h ApiKeyCreatedEvent) error) {
	m.onApiKeyCreatedEventHandler = h
}

func (m *ApiKeyEventHandler) onApiKeyCreated(c context.Context, e types.RawEvent) error {
	if m.onApiKeyCreatedEventHandler == nil {
		return nil
	}
	event := ApiKeyCreatedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onApiKeyCreatedEventHandler(c, event)
}

func (m *ApiKeyEventHandler) OnApiKeyDeleted(h func(c context.Context, h ApiKeyDeletedEvent) error) {
	m.onApiKeyDeletedEventHandler = h
}

func (m *ApiKeyEventHandler) onApiKeyDeleted(c context.Context, e types.RawEvent) error {
	if m.onApiKeyCreatedEventHandler == nil {
		return nil
	}
	event := ApiKeyDeletedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onApiKeyDeletedEventHandler(c, event)
}

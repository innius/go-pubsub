package apikeyevents

const (
	apiKeyCreatedEventName = "ApiKeyCreatedEvent"
	apiKeyDeletedEventName = "ApiKeyDeletedEvent"
)

const source = "apikeys"

type ApiKeyCreatedEvent struct {
	CompanyID string `json:"company_id"`
	Domain    string `json:"domain"`
	KeyID     string `json:"key_id"`
	Name      string `json:"name"`
}

func (e ApiKeyCreatedEvent) EventType() string {
	return apiKeyCreatedEventName
}

func (e ApiKeyCreatedEvent) EventSource() string {
	return source
}

type ApiKeyDeletedEvent struct {
	CompanyID string `json:"company_id"`
	Domain    string `json:"domain"`
	KeyID     string `json:"key_id"`
}

func (e ApiKeyDeletedEvent) EventType() string {
	return apiKeyDeletedEventName
}

func (e ApiKeyDeletedEvent) EventSource() string {
	return source
}

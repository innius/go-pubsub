package companyevents

import (
	"context"
	"encoding/json"
	"testing"

	"bitbucket.org/innius/go-pubsub/types"
	"github.com/stretchr/testify/assert"
)

func TestSubscriptions(t *testing.T) {
	tests := []types.EventMessage{
		CompanyCreatedEvent{},
		CompanyUpdatedEvent{},
		CompanyDeletedEvent{},
		AdminCreatedEvent{},
		AdminDeletedEvent{},
		PersonCreatedEvent{},
		PersonUpdatedEvent{},
		PersonDeletedEvent{},
		RelationVerifiedEvent{},
		RelationDeletedEvent{},
	}

	for _, evt := range tests {
		s := &CompanyEventHandler{}
		src := ""
		s.OnCompanyCreated(func(c context.Context, e CompanyCreatedEvent) error {
			src = e.EventType()
			return nil
		})
		s.OnCompanyUpdated(func(c context.Context, e CompanyUpdatedEvent) error {
			src = e.EventType()
			return nil
		})
		s.OnCompanyDeleted(func(c context.Context, e CompanyDeletedEvent) error {
			src = e.EventType()
			return nil
		})
		s.OnAdminCreated(func(c context.Context, e AdminCreatedEvent) error {
			src = e.EventType()
			return nil
		})
		s.OnAdminDeleted(func(c context.Context, e AdminDeletedEvent) error {
			src = e.EventType()
			return nil
		})

		s.OnPersonCreated(func(c context.Context, e PersonCreatedEvent) error {
			src = e.EventType()
			return nil
		})

		s.OnPersonUpdated(func(c context.Context, e PersonUpdatedEvent) error {
			src = e.EventType()
			return nil
		})

		s.OnPersonDeleted(func(c context.Context, e PersonDeletedEvent) error {
			src = e.EventType()
			return nil
		})

		s.OnRelationVerified(func(c context.Context, e RelationVerifiedEvent) error {
			src = e.EventType()
			return nil
		})

		s.OnRelationDeleted(func(c context.Context, e RelationDeletedEvent) error {
			src = e.EventType()
			return nil
		})

		bits, err := json.Marshal(evt)
		assert.NoError(t, err)
		raw := types.RawEvent{
			EventType:   evt.EventType(),
			EventSource: evt.EventSource(),
			RawMessage:  bits,
		}
		assert.NoError(t, s.EventHandler(context.Background(), raw))
		assert.Equal(t, evt.EventType(), src)
	}
}

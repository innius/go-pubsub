package companyevents

const (
	companyCreatedEventName   = "CompanyCreatedEvent"
	companyUpdatedEventName   = "CompanyUpdatedEvent"
	companyDeletedEventName   = "CompanyDeletedEvent"
	personCreatedEventName    = "PersonCreatedEvent"
	personUpdatedEventName    = "PersonUpdatedEvent"
	personDeletedEventName    = "PersonDeletedEvent"
	adminCreatedEventName     = "AdminCreatedEvent"
	adminDeletedEventName     = "AdminDeletedEvent"
	relationVerifiedEventName = "RelationVerifiedEvent"
	relationDeletedEventName  = "RelationDeletedEvent"
)

const source = "companies"

// CompanyCreatedEvent is sent if a new company is created
type CompanyCreatedEvent struct {
	CompanyID string `json:"company_id"`
	Domain    string `json:"domain"`
	Name      string `json:"name"`
	Address   string `json:"address"`
	Zipcode   string `json:"zipcode"`
	City      string `json:"city"`
	Country   string `json:"country"`
}

func (e CompanyCreatedEvent) EventType() string {
	return companyCreatedEventName
}

func (e CompanyCreatedEvent) EventSource() string {
	return source
}

// CompanyUpdatedEvent is sent if a company is updated
type CompanyUpdatedEvent struct {
	CompanyID string `json:"company_id"`
	Domain    string `json:"domain"`
	Name      string `json:"name"`
	Address   string `json:"address"`
	Zipcode   string `json:"zipcode"`
	City      string `json:"city"`
	Country   string `json:"country"`
}

func (e CompanyUpdatedEvent) EventType() string {
	return companyUpdatedEventName
}

func (e CompanyUpdatedEvent) EventSource() string {
	return source
}

// CompanyDeletedEvent is sent if a company is deleted
type CompanyDeletedEvent struct {
	CompanyID string   `json:"company_id"`
	Domain    string   `json:"domain"`
	Name      string   `json:"name"`
	Machines  []string `json:"machines"`
}

func (e CompanyDeletedEvent) EventType() string {
	return companyDeletedEventName
}

func (e CompanyDeletedEvent) EventSource() string {
	return source
}

// PersonDeletedEvent is sent if the specified person is deleted
type PersonDeletedEvent struct {
	CompanyID string `json:"company_id"`
	PersonID  string `json:"person_id"`
	Email     string `json:"email"`
	Name      string `json:"name"`
}

func (e PersonDeletedEvent) EventType() string {
	return personDeletedEventName
}

func (e PersonDeletedEvent) EventSource() string {
	return source
}

// PersonCreatedEvent is sent if the specified person is created
type PersonCreatedEvent struct {
	CompanyID string `json:"company_id"`
	PersonID  string `json:"person_id"`
	Email     string `json:"email"`
	Name      string `json:"name"`
}

func (e PersonCreatedEvent) EventType() string {
	return personCreatedEventName
}

func (e PersonCreatedEvent) EventSource() string {
	return source
}

// PersonUpdatedEvent is sent if the specified person is updated
type PersonUpdatedEvent struct {
	CompanyID string `json:"company_id"`
	PersonID  string `json:"person_id"`
	Email     string `json:"email"`
	Name      string `json:"name"`
}

func (e PersonUpdatedEvent) EventType() string {
	return personUpdatedEventName
}

func (e PersonUpdatedEvent) EventSource() string {
	return source
}

// AdminCreatedEvent is sent if a new admin is created
type AdminCreatedEvent struct {
	CompanyID string `json:"company_id"`
	PersonID  string `json:"person_id"`
	Email     string `json:"email"`
	Name      string `json:"name"`
}

func (e AdminCreatedEvent) EventType() string {
	return adminCreatedEventName
}

func (e AdminCreatedEvent) EventSource() string {
	return source
}

// AdminDeletedEvent is sent if an admin is deleted
type AdminDeletedEvent struct {
	CompanyID string `json:"company_id"`
	PersonID  string `json:"person_id"`
	Email     string `json:"email"`
	Name      string `json:"name"`
}

func (e AdminDeletedEvent) EventType() string {
	return adminDeletedEventName
}

func (e AdminDeletedEvent) EventSource() string {
	return source
}

type RelationVerifiedEvent struct {
	CompanyID          string `json:"company_id"`
	CompanyName        string `json:"company_name"`
	RelatedCompanyID   string `json:"related_company_id"`
	RelatedCompanyName string `json:"related_company_name"`
}

func (e RelationVerifiedEvent) EventType() string {
	return relationVerifiedEventName
}

func (e RelationVerifiedEvent) EventSource() string {
	return source
}

type RelationDeletedEvent struct {
	CompanyID1 string `json:"company_id_1"`
	CompanyID2 string `json:"company_id_2"`
}

func (e RelationDeletedEvent) EventType() string {
	return relationDeletedEventName
}

func (e RelationDeletedEvent) EventSource() string {
	return source
}

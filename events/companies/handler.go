package companyevents

import (
	"context"
	"encoding/json"

	"bitbucket.org/innius/go-pubsub/types"
	"github.com/sirupsen/logrus"
)

type CompanyEventHandler struct {
	onCompanyCreatedHandler   func(context.Context, CompanyCreatedEvent) error
	onCompanyUpdatedHandler   func(context.Context, CompanyUpdatedEvent) error
	onCompanyDeletedHandler   func(context.Context, CompanyDeletedEvent) error
	onPersonCreatedHandler    func(context.Context, PersonCreatedEvent) error
	onPersonUpdatedHandler    func(context.Context, PersonUpdatedEvent) error
	onPersonDeletedHandler    func(context.Context, PersonDeletedEvent) error
	onAdminCreatedHandler     func(context.Context, AdminCreatedEvent) error
	onAdminDeletedHandler     func(context.Context, AdminDeletedEvent) error
	onRelationVerifiedHandler func(context.Context, RelationVerifiedEvent) error
	onRelationDeletedHandler  func(context.Context, RelationDeletedEvent) error
}

func (m *CompanyEventHandler) EventHandler(c context.Context, e types.RawEvent) error {
	const legacySource = "machine" // company events were previously part of the machine source namespace
	if e.EventSource != source && e.EventSource != legacySource {
		return nil
	}

	switch e.EventType {
	case companyCreatedEventName:
		return m.onCompanyCreated(c, e)
	case companyUpdatedEventName:
		return m.onCompanyUpdated(c, e)
	case companyDeletedEventName:
		return m.onCompanyDeleted(c, e)
	case personCreatedEventName:
		return m.onPersonCreated(c, e)
	case personUpdatedEventName:
		return m.onPersonUpdated(c, e)
	case personDeletedEventName:
		return m.onPersonDeleted(c, e)
	case adminCreatedEventName:
		return m.onAdminCreated(c, e)
	case adminDeletedEventName:
		return m.onAdminDeleted(c, e)
	case relationVerifiedEventName:
		return m.onRelationVerified(c, e)
	case relationDeletedEventName:
		return m.onRelationDeleted(c, e)
	default:
		logrus.Warnf("unsupported event type %v", e.EventType)
	}
	return nil
}

// OnCompanyCreated registers a handler for CompanyCreatedEvent
func (m *CompanyEventHandler) OnCompanyCreated(h func(c context.Context, h CompanyCreatedEvent) error) {
	m.onCompanyCreatedHandler = h
}

func (m *CompanyEventHandler) onCompanyCreated(c context.Context, e types.RawEvent) error {
	if m.onCompanyCreatedHandler == nil {
		return nil
	}
	event := CompanyCreatedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onCompanyCreatedHandler(c, event)
}

// OnCompanyUpdated registers a handler for CompanyUpdatedEvent
func (m *CompanyEventHandler) OnCompanyUpdated(h func(c context.Context, h CompanyUpdatedEvent) error) {
	m.onCompanyUpdatedHandler = h
}

func (m *CompanyEventHandler) onCompanyUpdated(c context.Context, e types.RawEvent) error {
	if m.onCompanyUpdatedHandler == nil {
		return nil
	}
	event := CompanyUpdatedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onCompanyUpdatedHandler(c, event)
}

// OnCompanyDeleted registers a handler for CompanyDeletedEvent
func (m *CompanyEventHandler) OnCompanyDeleted(h func(c context.Context, h CompanyDeletedEvent) error) {
	m.onCompanyDeletedHandler = h
}

func (m *CompanyEventHandler) onCompanyDeleted(c context.Context, e types.RawEvent) error {
	if m.onCompanyDeletedHandler == nil {
		return nil
	}
	event := CompanyDeletedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onCompanyDeletedHandler(c, event)
}

// OnPersonDeleted registers a handler for PersonDeletedEvent
func (m *CompanyEventHandler) OnPersonDeleted(h func(c context.Context, h PersonDeletedEvent) error) {
	m.onPersonDeletedHandler = h
}

func (m *CompanyEventHandler) onPersonDeleted(c context.Context, e types.RawEvent) error {
	if m.onPersonDeletedHandler == nil {
		return nil
	}
	event := PersonDeletedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onPersonDeletedHandler(c, event)
}

// OnPersonCreated registers a handler for PersonCreatedEvent
func (m *CompanyEventHandler) OnPersonCreated(h func(c context.Context, h PersonCreatedEvent) error) {
	m.onPersonCreatedHandler = h
}

func (m *CompanyEventHandler) onPersonCreated(c context.Context, e types.RawEvent) error {
	if m.onPersonCreatedHandler == nil {
		return nil
	}
	event := PersonCreatedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onPersonCreatedHandler(c, event)
}

// OnPersonUpdated registers a handler for PersonUpdatedEvent
func (m *CompanyEventHandler) OnPersonUpdated(h func(c context.Context, h PersonUpdatedEvent) error) {
	m.onPersonUpdatedHandler = h
}

func (m *CompanyEventHandler) onPersonUpdated(c context.Context, e types.RawEvent) error {
	if m.onPersonUpdatedHandler == nil {
		return nil
	}
	event := PersonUpdatedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onPersonUpdatedHandler(c, event)
}

// OnAdminDeleted registers a handler for AdminDeletedEvent
func (m *CompanyEventHandler) OnAdminDeleted(h func(c context.Context, h AdminDeletedEvent) error) {
	m.onAdminDeletedHandler = h
}

func (m *CompanyEventHandler) onAdminDeleted(c context.Context, e types.RawEvent) error {
	if m.onAdminDeletedHandler == nil {
		return nil
	}
	event := AdminDeletedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onAdminDeletedHandler(c, event)
}

// OnAdminCreated registers a handler for AdminCreatedEvent
func (m *CompanyEventHandler) OnAdminCreated(h func(c context.Context, h AdminCreatedEvent) error) {
	m.onAdminCreatedHandler = h
}

func (m *CompanyEventHandler) onAdminCreated(c context.Context, e types.RawEvent) error {
	if m.onAdminCreatedHandler == nil {
		return nil
	}
	event := AdminCreatedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onAdminCreatedHandler(c, event)
}

// OnRelationVerified registers a handler for RelationVerifiedEventEvent
func (m *CompanyEventHandler) OnRelationVerified(h func(c context.Context, h RelationVerifiedEvent) error) {
	m.onRelationVerifiedHandler = h
}

func (m *CompanyEventHandler) onRelationVerified(c context.Context, e types.RawEvent) error {
	if m.onRelationVerifiedHandler == nil {
		return nil
	}
	event := RelationVerifiedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onRelationVerifiedHandler(c, event)
}

// OnRelationDeleted registers a handler for RelationDeletedEvent
func (m *CompanyEventHandler) OnRelationDeleted(h func(c context.Context, h RelationDeletedEvent) error) {
	m.onRelationDeletedHandler = h
}

func (m *CompanyEventHandler) onRelationDeleted(c context.Context, e types.RawEvent) error {
	if m.onRelationDeletedHandler == nil {
		return nil
	}
	event := RelationDeletedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onRelationDeletedHandler(c, event)
}

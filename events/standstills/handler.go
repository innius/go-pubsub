package standstills

import (
	"context"
	"encoding/json"

	"bitbucket.org/innius/go-pubsub/types"
	"github.com/sirupsen/logrus"
)

type StandstillsEventHandler struct {
	onStandstillTrackingPermBlockHandler func(context.Context, StandstillTrackingPermBlockEvent) error
	onStandstillTrackingTempBlockHandler func(context.Context, StandstillTrackingTempBlockEvent) error
}

func (m *StandstillsEventHandler) EventHandler(c context.Context, e types.RawEvent) error {
	if e.EventSource != source {
		return nil
	}

	switch e.EventType {
	case standstillTrackingPermBlockEventName:
		return m.onStandstillTrackingPermBlock(c, e)
	case standstillTrackingTempBlockEventName:
		return m.onStandstillTrackingTempBlock(c, e)
	default:
		logrus.Warnf("unsupported event type %v", e.EventType)
		return nil
	}
}

// OnMachineConnected registers a handler for MachineConnectedEvent
func (m *StandstillsEventHandler) OnStandstillTrackingTempBlock(h func(c context.Context, h StandstillTrackingTempBlockEvent) error) {
	m.onStandstillTrackingTempBlockHandler = h
}

func (m *StandstillsEventHandler) onStandstillTrackingTempBlock(c context.Context, e types.RawEvent) error {
	if m.onStandstillTrackingTempBlockHandler == nil {
		return nil
	}
	event := StandstillTrackingTempBlockEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onStandstillTrackingTempBlockHandler(c, event)
}

func (m *StandstillsEventHandler) OnStandstillTrackingPermBlock(h func(c context.Context, h StandstillTrackingPermBlockEvent) error) {
	m.onStandstillTrackingPermBlockHandler = h
}

func (m *StandstillsEventHandler) onStandstillTrackingPermBlock(c context.Context, e types.RawEvent) error {
	if m.onStandstillTrackingPermBlockHandler == nil {
		return nil
	}
	event := StandstillTrackingPermBlockEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onStandstillTrackingPermBlockHandler(c, event)
}

package standstills

import (
	"context"
	"encoding/json"
	"testing"

	"bitbucket.org/innius/go-pubsub/types"
	"github.com/stretchr/testify/assert"
)

func TestSubscriptions(t *testing.T) {
	tests := []types.EventMessage{
		StandstillTrackingPermBlockEvent{},
		StandstillTrackingTempBlockEvent{},
	}

	for _, evt := range tests {
		s := &StandstillsEventHandler{}
		src := ""
		s.OnStandstillTrackingPermBlock(func(c context.Context, e StandstillTrackingPermBlockEvent) error {
			src = e.EventType()
			return nil
		})
		s.OnStandstillTrackingTempBlock(func(c context.Context, e StandstillTrackingTempBlockEvent) error {
			src = e.EventType()
			return nil
		})

		bits, err := json.Marshal(evt)
		assert.NoError(t, err)
		raw := types.RawEvent{
			EventType:   evt.EventType(),
			EventSource: evt.EventSource(),
			RawMessage:  bits,
		}
		assert.NoError(t, s.EventHandler(context.Background(), raw))
		assert.Equal(t, evt.EventType(), src)
	}
}

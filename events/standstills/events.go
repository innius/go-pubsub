package standstills

const source = "standstills"

const (
	standstillTrackingPermBlockEventName = "StandstillTrackingPermBlock"
	standstillTrackingTempBlockEventName = "StandstillTrackingTempBlock"
)

// StandstillTrackingTemporarlyBlock is sent when a machine-status-sensor is temporarily blocked for that 29/04/2024
type StandstillTrackingTempBlockEvent struct {
	MachineID     string `json:"machinetrn"`
	MS3ID         string `json:"ms3id"`
	MaxPerDay     int64  `json:"max_per_day"`
	TimestampUnix int64  `json:"timestamp_unix"`
}

func (e StandstillTrackingTempBlockEvent) EventType() string {
	return standstillTrackingTempBlockEventName
}

func (e StandstillTrackingTempBlockEvent) EventSource() string {
	return source
}

// StandstillTrackingPermBlockEvent is sent when a machine is connected
type StandstillTrackingPermBlockEvent struct {
	MachineID     string `json:"machinetrn"`
	MS3ID         string `json:"ms3id"`
	MaxPerDay     int64  `json:"max_per_day"`
	MaxDays       int64  `json:"max_days"`
	TimestampUnix int64  `json:"timestamp_unix"`
}

func (e StandstillTrackingPermBlockEvent) EventType() string {
	return standstillTrackingPermBlockEventName
}

func (e StandstillTrackingPermBlockEvent) EventSource() string {
	return source
}

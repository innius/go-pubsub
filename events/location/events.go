package locationevents

const (
	machineLocationChangedEventName = "MachineLocationChangedEvent"
)

// MachineLocationChangedEvent is sent when a machine's location is changed
type MachineLocationChangedEvent struct {
	ID      string `json:"id"`
	Address string `json:"address"`
}

func (e MachineLocationChangedEvent) EventType() string {
	return machineLocationChangedEventName
}

func (e MachineLocationChangedEvent) EventSource() string {
	return source
}

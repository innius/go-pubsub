package locationevents

import (
	"context"
	"encoding/json"

	"bitbucket.org/innius/go-pubsub/types"
	"github.com/sirupsen/logrus"
)

type LocationEventHandler struct {
	onMachineLocationChangedHandler func(context.Context, MachineLocationChangedEvent) error
}

const source = "locations"

func (m *LocationEventHandler) EventHandler(c context.Context, e types.RawEvent) error {
	if e.EventSource != source {
		return nil
	}

	switch e.EventType {
	case machineLocationChangedEventName:
		return m.onMachineLocationChanged(c, e)
	default:
		logrus.Warnf("unsupported event type %v", e.EventType)
	}
	return nil
}

// OnMachineLocationChanged registers a handler for MachineLocationChangedEvent
func (m *LocationEventHandler) OnMachineLocationChanged(h func(c context.Context, h MachineLocationChangedEvent) error) {
	m.onMachineLocationChangedHandler = h
}

func (m *LocationEventHandler) onMachineLocationChanged(c context.Context, e types.RawEvent) error {
	if m.onMachineLocationChangedHandler == nil {
		return nil
	}
	event := MachineLocationChangedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onMachineLocationChangedHandler(c, event)
}


package connection

import (
	"context"
	"encoding/json"
	"testing"

	"bitbucket.org/innius/go-pubsub/types"
	"github.com/stretchr/testify/assert"
)

func TestSubscriptions(t *testing.T) {
	tests := []types.EventMessage{
		MachineConnectedEvent{},
		MachineDisconnectedEvent{},
	}

	for _, evt := range tests {
		s := &ConnectionEventHandler{}
		src := ""
		s.OnMachineConnected(func(c context.Context, e MachineConnectedEvent) error {
			src = e.EventType()
			return nil
		})
		s.OnMachineDisconnected(func(c context.Context, e MachineDisconnectedEvent) error {
			src = e.EventType()
			return nil
		})

		bits, err := json.Marshal(evt)
		assert.NoError(t, err)
		raw := types.RawEvent{
			EventType:   evt.EventType(),
			EventSource: evt.EventSource(),
			RawMessage:  bits,
		}
		assert.NoError(t, s.EventHandler(context.Background(), raw))
		assert.Equal(t, evt.EventType(), src)
	}
}

package connection

const source = "connection"

const (
	machineConnectedEventName    = "MachineConnected"
	machineDisconnectedEventName = "MachineDisconnected"
)

// MachineConnectedEvent is sent when a machine is connected
type MachineConnectedEvent struct {
	MachineID string `json:"machinetrn"`
	Timestamp int64  `json:"timestamp"`
}

func (e MachineConnectedEvent) EventType() string {
	return machineConnectedEventName
}

func (e MachineConnectedEvent) EventSource() string {
	return source
}

// MachineConnectedEvent is sent when a machine is disconnected
type MachineDisconnectedEvent struct {
	MachineID     string `json:"machinetrn"`
	LastHeartBeat int64  `json:"last_heart_beat"`
	Timestamp     int64  `json:"timestamp"`
}

func (e MachineDisconnectedEvent) EventType() string {
	return machineDisconnectedEventName
}

func (e MachineDisconnectedEvent) EventSource() string {
	return source
}
package connection

import (
	"context"
	"encoding/json"

	"bitbucket.org/innius/go-pubsub/types"
	"github.com/sirupsen/logrus"
)

type ConnectionEventHandler struct {
	onMachineConnectedHandler    func(context.Context, MachineConnectedEvent) error
	onMachineDisconnectedHandler func(context.Context, MachineDisconnectedEvent) error
}

func (m *ConnectionEventHandler) EventHandler(c context.Context, e types.RawEvent) error {
	if e.EventSource != source {
		return nil
	}

	switch e.EventType {
	case machineConnectedEventName:
		return m.onMachineConnected(c, e)
	case machineDisconnectedEventName:
		return m.onMachineDisconnected(c, e)
	default:
		logrus.Warnf("unsupported event type %v", e.EventType)
		return nil
	}
}

// OnMachineConnected registers a handler for MachineConnectedEvent
func (m *ConnectionEventHandler) OnMachineConnected(h func(c context.Context, h MachineConnectedEvent) error) {
	m.onMachineConnectedHandler = h
}

func (m *ConnectionEventHandler) onMachineConnected(c context.Context, e types.RawEvent) error {
	if m.onMachineConnectedHandler == nil {
		return nil
	}
	event := MachineConnectedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onMachineConnectedHandler(c, event)
}

// OnMachineConnected registers a handler for MachineConnectedEvent
func (m *ConnectionEventHandler) OnMachineDisconnected(h func(c context.Context, h MachineDisconnectedEvent) error) {
	m.onMachineDisconnectedHandler = h
}

func (m *ConnectionEventHandler) onMachineDisconnected(c context.Context, e types.RawEvent) error {
	if m.onMachineDisconnectedHandler == nil {
		return nil
	}
	event := MachineDisconnectedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onMachineDisconnectedHandler(c, event)
}


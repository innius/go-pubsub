package activity

import (
	"bitbucket.org/innius/go-pubsub/types"
	"context"
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSubscriptions(t *testing.T) {
	tests := []types.EventMessage{
		ActivityCreatedEvent{},
		ActivityStatusChangedEvent{},
		ActivityAssigneeChangedEvent{},
	}

	h := &ActivityEventHandler{}
	src := ""
	h.OnActivityCreated(func(c context.Context, e ActivityCreatedEvent) error {
		src = e.EventType()
		return nil
	})
	h.OnActivityStatusChanged(func(c context.Context, e ActivityStatusChangedEvent) error {
		src = e.EventType()
		return nil
	})
	h.OnActivityAssigneeChanged(func(c context.Context, e ActivityAssigneeChangedEvent) error {
		src = e.EventType()
		return nil
	})

	for _, evt := range tests {
		bits, err := json.Marshal(evt)
		assert.NoError(t, err)
		raw := types.RawEvent{
			EventType:   evt.EventType(),
			EventSource: evt.EventSource(),
			RawMessage:  bits,
		}
		assert.NoError(t, h.EventHandler(context.Background(), raw))
		assert.Equal(t, evt.EventType(), src)
	}
}

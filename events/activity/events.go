package activity

const source = "activity"

const (
	activityCreatedEventName         = "ActivityCreatedEvent"
	activityStatusChangedEventName   = "ActivityStatusChangedEvent"
	activityAssigneeChangedEventName = "ActivityAssigneeChangedEvent"
)

type ActivityStatus int
const (
	Unknown ActivityStatus = iota
	ToDo
	InProgress
	Done
	Cancelled
	Postponed
)

func (s ActivityStatus) String() string {
	switch s {
	case ToDo:
		return "To Do"
	case InProgress:
		return "In Progress"
	case Done:
		return "Done"
	case Cancelled:
		return "Cancelled"
	case Postponed:
		return "Postponed"
	default:
		return "Unknown"
	}
	return "Unknown"
}

type ActivityCreatedEvent struct {
	Machinetrn     string `json:"machinetrn"`
	Id             string `json:"activity_id"`
	Type           string `json:"activity_type"`
	ScheduledStart int64  `json:"scheduled_start"`
	ScheduledEnd   int64  `json:"scheduled_end"`
	Assignee       string `json:"assignee"`
	Title          string `json:"title"`
	Description    string `json:"description"`
	Timestamp      int64  `json:"timestamp"`
}

func (e ActivityCreatedEvent) EventType() string {
	return activityCreatedEventName
}

func (e ActivityCreatedEvent) EventSource() string {
	return source
}

type ActivityStatusChangedEvent struct {
	Machinetrn     string         `json:"machinetrn"`
	Id             string         `json:"activity_id"`
	Title          string         `json:"title"`
	Status         ActivityStatus `json:"status"`
	Timestamp      int64          `json:"timestamp"`
}

func (e ActivityStatusChangedEvent) EventType() string {
	return activityStatusChangedEventName
}

func (e ActivityStatusChangedEvent) EventSource() string {
	return source
}

type ActivityAssigneeChangedEvent struct {
	Machinetrn     string `json:"machinetrn"`
	Id             string `json:"activity_id"`
	Title          string `json:"title"`
	Assignee       string `json:"assignee"`
	AssigneeName   string `json:"assignee_name"` //TODO implement in sender
	Timestamp      int64  `json:"timestamp"`
}

func (e ActivityAssigneeChangedEvent) EventType() string {
	return activityAssigneeChangedEventName
}

func (e ActivityAssigneeChangedEvent) EventSource() string {
	return source
}

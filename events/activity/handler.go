package activity

import (
	"bitbucket.org/innius/go-pubsub/types"
	"context"
	"encoding/json"
	"github.com/sirupsen/logrus"
)

type ActivityEventHandler struct {
	onActivityCreatedEventHandler         func(context.Context, ActivityCreatedEvent) error
	onActivityStatusChangedEventHandler   func(context.Context, ActivityStatusChangedEvent) error
	onActivityAssigneeChangedEventHandler func(context.Context, ActivityAssigneeChangedEvent) error
}

func (h *ActivityEventHandler) EventHandler(c context.Context, e types.RawEvent) error {
	if e.EventSource != source {
		return nil
	}

	switch e.EventType {
	case activityCreatedEventName:
		return h.onActivityCreated(c, e)
	case activityStatusChangedEventName:
		return h.onActivityStatusChanged(c, e)
	case activityAssigneeChangedEventName:
		return h.onActivityAssigneeChanged(c, e)
	default:
		logrus.Warnf("unsupported event type %v", e.EventType)
	}
	return nil
}


func (h *ActivityEventHandler) OnActivityCreated(f func(context.Context, ActivityCreatedEvent) error) {
	h.onActivityCreatedEventHandler = f
}

func (h *ActivityEventHandler) OnActivityStatusChanged(f func(context.Context, ActivityStatusChangedEvent) error) {
	h.onActivityStatusChangedEventHandler = f
}

func (h *ActivityEventHandler) OnActivityAssigneeChanged(f func(context.Context, ActivityAssigneeChangedEvent) error) {
	h.onActivityAssigneeChangedEventHandler = f
}


func (h *ActivityEventHandler) onActivityCreated(c context.Context, e types.RawEvent) error {
	if h.onActivityCreatedEventHandler == nil {
		return nil
	}
	event := ActivityCreatedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return h.onActivityCreatedEventHandler(c, event)
}

func (h *ActivityEventHandler) onActivityStatusChanged(c context.Context, e types.RawEvent) error {
	if h.onActivityStatusChangedEventHandler == nil {
		return nil
	}
	event := ActivityStatusChangedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}
	return h.onActivityStatusChangedEventHandler(c, event)
}

func (h *ActivityEventHandler) onActivityAssigneeChanged(c context.Context, e types.RawEvent) error {
	if h.onActivityAssigneeChangedEventHandler == nil {
		return nil
	}
	event := ActivityAssigneeChangedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}
	return h.onActivityAssigneeChangedEventHandler(c, event)
}

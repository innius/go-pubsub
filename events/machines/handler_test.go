package machineevents

import (
	"context"
	"encoding/json"
	"testing"

	"bitbucket.org/innius/go-pubsub/types"
	"github.com/stretchr/testify/assert"
)

func TestSubscriptions(t *testing.T) {
	tests := []types.EventMessage{
		MachineCreatedEvent{},
		MachineDeletedEvent{},
		MachineNameUpdatedEvent{},
		MachineSharedEvent{},
		MachineUnsharedEvent{},
		MachineDescriptionUpdatedEvent{},
	}

	for _, evt := range tests {
		s := &MachineEventHandler{}
		src := ""

		setSource := func(eventType string) error {
			src = eventType
			return nil
		}

		s.OnMachineCreated(func(c context.Context, e MachineCreatedEvent) error {
			return setSource(e.EventType())
		})
		s.OnMachineDeleted(func(c context.Context, e MachineDeletedEvent) error {
			return setSource(e.EventType())
		})
		s.OnMachineNameUpdated(func(c context.Context, e MachineNameUpdatedEvent) error {
			return setSource(e.EventType())
		})
		s.OnMachineShared(func(c context.Context, e MachineSharedEvent) error {
			return setSource(e.EventType())
		})
		s.OnMachineUnshared(func(c context.Context, e MachineUnsharedEvent) error {
			return setSource(e.EventType())
		})
		s.OnMachineDescriptionUpdated(func(c context.Context, e MachineDescriptionUpdatedEvent) error {
			return setSource(e.EventType())
		})

		bits, err := json.Marshal(evt)
		assert.NoError(t, err)
		raw := types.RawEvent{
			EventType:   evt.EventType(),
			EventSource: evt.EventSource(),
			RawMessage:  bits,
		}
		assert.NoError(t, s.EventHandler(context.Background(), raw))
		assert.Equal(t, evt.EventType(), src)
	}
}

package machineevents

import (
	"context"
	"encoding/json"

	"bitbucket.org/innius/go-pubsub/types"
	"github.com/sirupsen/logrus"
)

type MachineEventHandler struct {
	onMachineSharedHandler             func(context.Context, MachineSharedEvent) error
	onMachineUnsharedHandler           func(context.Context, MachineUnsharedEvent) error
	onMachineCreatedHandler            func(context.Context, MachineCreatedEvent) error
	onMachineDeletedHandler            func(context.Context, MachineDeletedEvent) error
	onMachineNameUpdatedHandler        func(context.Context, MachineNameUpdatedEvent) error
	onMachineDescriptionUpdatedHandler func(context.Context, MachineDescriptionUpdatedEvent) error
}

const source = "machines"
const legacySource = "machine"

func (m *MachineEventHandler) EventHandler(c context.Context, e types.RawEvent) error {
	if e.EventSource != legacySource && e.EventSource != source {
		return nil
	}

	switch e.EventType {
	case machineSharedEventName:
		return m.onMachineShared(c, e)
	case machineUnsharedEventName:
		return m.onMachineUnshared(c, e)
	case machineCreatedEventName:
		return m.onMachineCreated(c, e)
	case machineDeletedEventName:
		return m.onMachineDeleted(c, e)
	case machineNameUpdatedEventName:
		return m.onMachineNameUpdated(c, e)
	case machineDescriptionUpdatedEventName:
		return m.onMachineDescriptionUpdated(c, e)
	default:
		logrus.Warnf("unsupported event type %v", e.EventType)
	}
	return nil
}

// OnMachineShared registers a handler for MachineSharedEvent
func (m *MachineEventHandler) OnMachineShared(h func(c context.Context, h MachineSharedEvent) error) {
	m.onMachineSharedHandler = h
}

func (m *MachineEventHandler) onMachineShared(c context.Context, e types.RawEvent) error {
	if m.onMachineSharedHandler == nil {
		return nil
	}
	event := MachineSharedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onMachineSharedHandler(c, event)
}

// OnMachineUnshared registers a handler for MachineUnsharedEvent
func (m *MachineEventHandler) OnMachineUnshared(h func(c context.Context, h MachineUnsharedEvent) error) {
	m.onMachineUnsharedHandler = h
}

func (m *MachineEventHandler) onMachineUnshared(c context.Context, e types.RawEvent) error {
	if m.onMachineUnsharedHandler == nil {
		return nil
	}
	event := MachineUnsharedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onMachineUnsharedHandler(c, event)
}

// OnMachineCreated registers a handler for MachineCreatedEvent
func (m *MachineEventHandler) OnMachineCreated(h func(c context.Context, h MachineCreatedEvent) error) {
	m.onMachineCreatedHandler = h
}

func (m *MachineEventHandler) onMachineCreated(c context.Context, e types.RawEvent) error {
	if m.onMachineCreatedHandler == nil {
		return nil
	}
	event := MachineCreatedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onMachineCreatedHandler(c, event)
}

// OnMachineDeleted registers a handler for MachineDeletedEvent
func (m *MachineEventHandler) OnMachineDeleted(h func(c context.Context, h MachineDeletedEvent) error) {
	m.onMachineDeletedHandler = h
}

func (m *MachineEventHandler) onMachineDeleted(c context.Context, e types.RawEvent) error {
	if m.onMachineDeletedHandler == nil {
		return nil
	}
	event := MachineDeletedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onMachineDeletedHandler(c, event)
}

// OnMachineNameUpdated registers a handler for MachineNameUpdatedEvent
func (m *MachineEventHandler) OnMachineNameUpdated(h func(c context.Context, h MachineNameUpdatedEvent) error) {
	m.onMachineNameUpdatedHandler = h
}

func (m *MachineEventHandler) onMachineNameUpdated(c context.Context, e types.RawEvent) error {
	if m.onMachineNameUpdatedHandler == nil {
		return nil
	}
	event := MachineNameUpdatedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onMachineNameUpdatedHandler(c, event)
}

// OnMachineNameUpdated registers a handler for MachineDescriptionUpdatedEvent
func (m *MachineEventHandler) OnMachineDescriptionUpdated(h func(c context.Context, h MachineDescriptionUpdatedEvent) error) {
	m.onMachineDescriptionUpdatedHandler = h
}

func (m *MachineEventHandler) onMachineDescriptionUpdated(c context.Context, e types.RawEvent) error {
	if m.onMachineDescriptionUpdatedHandler == nil {
		return nil
	}
	event := MachineDescriptionUpdatedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onMachineDescriptionUpdatedHandler(c, event)
}

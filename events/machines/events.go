package machineevents

const (
	machineCreatedEventName            = "MachineCreatedEvent"
	machineDeletedEventName            = "MachineDeletedEvent"
	machineNameUpdatedEventName        = "MachineNameUpdatedEvent"
	machineSharedEventName             = "MachineSharedEvent"
	machineUnsharedEventName           = "MachineUnsharedEvent"
	machineDescriptionUpdatedEventName = "MachineDescriptionUpdatedEvent"
)

// MachineCreatedEvent is sent if a new machine is created
type MachineCreatedEvent struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

func (e MachineCreatedEvent) EventType() string {
	return machineCreatedEventName
}

func (e MachineCreatedEvent) EventSource() string {
	return source
}

// MachineDeletedEvent is sent if a machine is deleted
type MachineDeletedEvent struct {
	ID string `json:"id"`
}

func (e MachineDeletedEvent) EventType() string {
	return machineDeletedEventName
}

func (e MachineDeletedEvent) EventSource() string {
	return source
}

// MachineNameUpdatedEvent is sent if a machine's name is updated
type MachineNameUpdatedEvent struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

func (e MachineNameUpdatedEvent) EventType() string {
	return machineNameUpdatedEventName
}

func (e MachineNameUpdatedEvent) EventSource() string {
	return source
}

type MachineSharedEvent struct {
	MachineID  string `json:"machine_id"`
	CompanyID  string `json:"company_id"`
	SharedWith string `json:"shared_with"`
}

func (e MachineSharedEvent) EventType() string {
	return machineSharedEventName
}

func (e MachineSharedEvent) EventSource() string {
	return source
}

type MachineUnsharedEvent struct {
	MachineID  string `json:"machine_id,omitempty"`
	CompanyID  string `json:"company_id,omitempty"`
	SharedWith string `json:"shared_with,omitempty"`
}

func (e MachineUnsharedEvent) EventType() string {
	return machineUnsharedEventName
}

func (e MachineUnsharedEvent) EventSource() string {
	return source
}

type MachineDescriptionUpdatedEvent struct {
	MachineID      string `json:"machine_id,omitempty"`
	CompanyID      string `json:"company_id,omitempty"`
	NewDescription string `json:"new_description"`
}

func (e MachineDescriptionUpdatedEvent) EventType() string {
	return machineDescriptionUpdatedEventName
}

func (e MachineDescriptionUpdatedEvent) EventSource() string {
	return source
}

package monitoring

import "bitbucket.org/innius/go-pubsub/messaging"

const source = "monitoring"

const (
	machineStateChangeEventName = "MachineStateChangeEvent"
	sensorStateChangeEventName  = "SensorStateChangeEvent"
)

type MachineStateChangeEvent struct {
	MachineID  string                  `json:"machine_id"`
	Status     messaging.MessageStatus `json:"status"`
	SensorName string                  `json:"sensor_name"`
	Timestamp  int64                   `json:"timestamp"`
}

func (e MachineStateChangeEvent) EventType() string {
	return machineStateChangeEventName
}

func (e MachineStateChangeEvent) EventSource() string {
	return source
}

type SensorStateChangeEvent struct {
	MachineID  string                  `json:"machine_id"`
	SensorID   string                  `json:"sensor_id"`
	Status     messaging.MessageStatus `json:"status"`
	Decimals   int                     `json:"decimals"`
	Value      float64                 `json:"value"`
	Unit       string                  `json:"unit"`
	SensorName string                  `json:"sensor_name"`
	Timestamp  int64                   `json:"timestamp"`
	Push       bool                    `json:"push"`
}

func (e SensorStateChangeEvent) EventType() string {
	return sensorStateChangeEventName
}

func (e SensorStateChangeEvent) EventSource() string {
	return source
}

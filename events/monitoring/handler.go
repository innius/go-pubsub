package monitoring

import (
	"context"
	"encoding/json"

	"bitbucket.org/innius/go-pubsub/types"
	"github.com/sirupsen/logrus"
)

type MonitoringEventHandler struct {
	onMachineStateChangedHandler func(context.Context, MachineStateChangeEvent) error
	onSensorStateChangedHandler  func(context.Context, SensorStateChangeEvent) error
}

func (m *MonitoringEventHandler) EventHandler(c context.Context, e types.RawEvent) error {
	if e.EventSource != source {
		return nil
	}

	switch e.EventType {
	case machineStateChangeEventName:
		return m.onMachineStateChanged(c, e)
	case sensorStateChangeEventName:
		return m.onSensorStateChanged(c, e)
	default:
		logrus.Warnf("unsupported event type %v", e.EventType)
	}
	return nil
}

func (m *MonitoringEventHandler) OnMachineStateChanged(h func(c context.Context, h MachineStateChangeEvent) error) {
	m.onMachineStateChangedHandler = h
}

func (m *MonitoringEventHandler) onMachineStateChanged(c context.Context, e types.RawEvent) error {
	if m.onMachineStateChangedHandler == nil {
		return nil
	}
	event := MachineStateChangeEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onMachineStateChangedHandler(c, event)
}

func (m *MonitoringEventHandler) OnSensorStateChanged(h func(c context.Context, h SensorStateChangeEvent) error) {
	m.onSensorStateChangedHandler = h
}

func (m *MonitoringEventHandler) onSensorStateChanged(c context.Context, e types.RawEvent) error {
	if m.onSensorStateChangedHandler == nil {
		return nil
	}
	event := SensorStateChangeEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onSensorStateChangedHandler(c, event)
}

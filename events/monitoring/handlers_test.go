package monitoring

import (
	"context"
	"encoding/json"
	"testing"

	"bitbucket.org/innius/go-pubsub/types"
	"github.com/stretchr/testify/assert"
)

func TestSubscriptions(t *testing.T) {
	tests := []types.EventMessage{
		MachineStateChangeEvent{},
		SensorStateChangeEvent{},
	}

	for _, evt := range tests {
		s := &MonitoringEventHandler{}
		src := ""
		s.OnMachineStateChanged(func(c context.Context, e MachineStateChangeEvent) error {
			src = e.EventType()
			return nil
		})
		s.OnSensorStateChanged(func(c context.Context, e SensorStateChangeEvent) error {
			src = e.EventType()
			return nil
		})

		bits, err := json.Marshal(evt)
		assert.NoError(t, err)
		raw := types.RawEvent{
			EventType:   evt.EventType(),
			EventSource: evt.EventSource(),
			RawMessage:  bits,
		}
		assert.NoError(t, s.EventHandler(context.Background(), raw))
		assert.Equal(t, evt.EventType(), src)
	}
}

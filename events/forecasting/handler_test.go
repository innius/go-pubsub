package forecastingevents

import (
	"context"
	"encoding/json"
	"testing"

	"bitbucket.org/innius/go-pubsub/types"
	"github.com/stretchr/testify/assert"
)

func TestSubscriptions(t *testing.T) {
	evt := SensorForecastThresholdCrossedEvent{
		ThresholdValue: 67,
	}

	s := &ForecastingEventHandler{}
	value := 0.0

	s.OnSensorForecastThresholdCrossed(func(c context.Context, e SensorForecastThresholdCrossedEvent) error {
		value = e.ThresholdValue
		return nil
	})

	bits, err := json.Marshal(evt)
	assert.NoError(t, err)
	raw := types.RawEvent{
		EventType:   evt.EventType(),
		EventSource: evt.EventSource(),
		RawMessage:  bits,
	}
	assert.NoError(t, s.EventHandler(context.Background(), raw))
	assert.Equal(t, 67.0, value)

}

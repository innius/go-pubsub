package forecastingevents

import (
	"context"
	"encoding/json"

	"bitbucket.org/innius/go-pubsub/types"
	"github.com/sirupsen/logrus"
)

type ForecastingEventHandler struct {
	onForecastedSensorStateChange func(context.Context, SensorForecastThresholdCrossedEvent) error
}

func (m *ForecastingEventHandler) EventHandler(c context.Context, e types.RawEvent) error {
	if e.EventSource != source {
		return nil
	}

	switch e.EventType {
	case sensorForecastThresholdCrossedEventName:
		return m.onSensorForecastThresholdCrossed(c, e)
	default:
		logrus.Warnf("unsupported event type %v", e.EventType)
		return nil
	}
}

// OnSensorForecastThresholdCrossed registers a handler for SensorForecastThresholdCrossedEvent
func (m *ForecastingEventHandler) OnSensorForecastThresholdCrossed(h func(c context.Context, h SensorForecastThresholdCrossedEvent) error) {
	m.onForecastedSensorStateChange = h
}

func (m *ForecastingEventHandler) onSensorForecastThresholdCrossed(c context.Context, e types.RawEvent) error {
	if m.onForecastedSensorStateChange == nil {
		return nil
	}
	event := SensorForecastThresholdCrossedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onForecastedSensorStateChange(c, event)
}

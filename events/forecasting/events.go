package forecastingevents

const source = "forecasting"

const (
	sensorForecastThresholdCrossedEventName = "SensorForecastThresholdCrossed"
)

// SensorForecastThresholdCrossedEvent is sent when it is predicted that a sensor will cross a threshold. ExpectedCrossingTimestamp is always in the future
type SensorForecastThresholdCrossedEvent struct {
	MachineID                 string  `json:"machine_id"`
	SensorID                  string  `json:"sensor_id"`
	SensorName                string  `json:"sensor_name"`
	Timestamp                 int64   `json:"timestamp"`
	ExpectedCrossingTimestamp int64   `json:"expected_crossing_timestamp"`
	ThresholdValue            float64 `json:"value"`
}

func (e SensorForecastThresholdCrossedEvent) EventType() string {
	return sensorForecastThresholdCrossedEventName
}

func (e SensorForecastThresholdCrossedEvent) EventSource() string {
	return source
}

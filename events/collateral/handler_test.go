package collateralevents

import (
	"context"
	"encoding/json"
	"testing"

	"bitbucket.org/innius/go-pubsub/types"
	"github.com/stretchr/testify/assert"
)

func TestSubscriptions(t *testing.T) {
	tests := []types.EventMessage{
		CollateralItemUploadedEvent{},
		CollateralItemDeletedEvent{},
	}

	for _, evt := range tests {
		s := &CollateralEventHandler{}
		src := ""
		s.OnCollateralItemUploaded(func(c context.Context, e CollateralItemUploadedEvent) error {
			src = e.EventType()
			return nil
		})
		s.OnCollateralItemDeleted(func(c context.Context, e CollateralItemDeletedEvent) error {
			src = e.EventType()
			return nil
		})

		bits, err := json.Marshal(evt)
		assert.NoError(t, err)
		raw := types.RawEvent{
			EventType:   evt.EventType(),
			EventSource: evt.EventSource(),
			RawMessage:  bits,
		}
		assert.NoError(t, s.EventHandler(context.Background(), raw))
		assert.Equal(t, evt.EventType(), src)
	}

}

package collateralevents

import (
	"context"
	"encoding/json"

	"bitbucket.org/innius/go-pubsub/types"
	"github.com/sirupsen/logrus"
)

type CollateralEventHandler struct {
	onCollateralItemUploadedEventHandler func(context.Context, CollateralItemUploadedEvent) error
	onCollateralItemDeletedEventHandler  func(ctx context.Context, event CollateralItemDeletedEvent) error
}

func (m *CollateralEventHandler) EventHandler(c context.Context, e types.RawEvent) error {
	if e.EventSource != source {
		return nil
	}

	switch e.EventType {
	case collateralItemUploadedEventName:
		return m.onCollateralItemUploaded(c, e)
	case collateralItemDeletedEventName:
		return m.onCollateralItemDeleted(c, e)
	default:
		logrus.Warnf("unsupported event type %v", e.EventType)
		return nil
	}
}

// OnCollateralItemUploaded registers a handler for CollateralItemUploadedEvent
func (m *CollateralEventHandler) OnCollateralItemUploaded(h func(c context.Context, h CollateralItemUploadedEvent) error) {
	m.onCollateralItemUploadedEventHandler = h
}

func (m *CollateralEventHandler) onCollateralItemUploaded(c context.Context, e types.RawEvent) error {
	if m.onCollateralItemUploadedEventHandler == nil {
		return nil
	}
	event := CollateralItemUploadedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onCollateralItemUploadedEventHandler(c, event)
}

// OnCollateralItemDeleted registers a handler for CollateralItemDeletedEvent
func (m *CollateralEventHandler) OnCollateralItemDeleted(h func(c context.Context, h CollateralItemDeletedEvent) error) {
	m.onCollateralItemDeletedEventHandler = h
}

func (m *CollateralEventHandler) onCollateralItemDeleted(c context.Context, e types.RawEvent) error {
	if m.onCollateralItemDeletedEventHandler == nil {
		return nil
	}
	event := CollateralItemDeletedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onCollateralItemDeletedEventHandler(c, event)
}

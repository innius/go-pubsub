package collateralevents

const source = "collateral"

const (
	collateralItemUploadedEventName = "CollateralItemUploaded"
	collateralItemDeletedEventName  = "CollateralItemDeleted"
)

func (e CollateralItemUploadedEvent) EventType() string {
	return collateralItemUploadedEventName
}

func (e CollateralItemUploadedEvent) EventSource() string {
	return source
}

type CollateralItemUploadedEvent struct {
	MachineID string `json:"machineid"`
	Name      string `json:"itemname"`
	Timestamp int64  `json:"timestamp"`
}

func (e CollateralItemDeletedEvent) EventType() string {
	return collateralItemDeletedEventName
}

func (e CollateralItemDeletedEvent) EventSource() string {
	return source
}

type CollateralItemDeletedEvent struct {
	MachineID string `json:"machineid"`
	Name      string `json:"itemname"`
	Timestamp int64  `json:"timestamp"`
}

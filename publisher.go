package go_pubsub

import (
	"bitbucket.org/innius/go-pubsub/types"
	"context"
	"encoding/json"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/aws/aws-sdk-go/service/sns/snsiface"
	"github.com/pkg/errors"
)

type Publisher interface {
	// publish an event
	Publish(context.Context, types.EventMessage) error
}

func NewPublisher(client snsiface.SNSAPI, topicArn string, legacy bool) Publisher {
	return &snsPublisher{topicArn, client, legacy}
}

type snsPublisher struct {
	topicArn string
	client   snsiface.SNSAPI
	legacy   bool
}

func (p snsPublisher) Publish(c context.Context, e types.EventMessage) error {
	msg := &message{
		Message: e,
		RawEvent: &types.RawEvent{
			EventType:   e.EventType(),
			EventSource: e.EventSource(),
		},
	}
	if p.legacy {
		return p.publishWithLegacy(c, msg)
	}
	return p.publish(c, msg)
}

func (p *snsPublisher) publishWithLegacy(ctx context.Context, m *message) error {
	if m.EventSource == "companies" || m.EventSource == "machines" {
		m2 := *m

		re := *m.RawEvent
		m2.RawEvent = &re
		m2.EventSource = "machine"

		_ = p.publish(ctx, &m2)
	}
	return p.publish(ctx, m)
}

type message struct {
	*types.RawEvent
	Message types.EventMessage `json:"Message"`
}

func (p snsPublisher) publish(c context.Context, m *message) error {
	bits, err := json.Marshal(m.Message)
	if err != nil {
		return errors.Wrap(err, "Unable to marshal event")
	}
	_, err = p.client.PublishWithContext(c, &sns.PublishInput{
		Message: aws.String(string(bits)),
		MessageAttributes: map[string]*sns.MessageAttributeValue{
			types.MessageSourceAttr: {DataType: aws.String("String"), StringValue: aws.String(m.EventSource)},
			types.MessageTypeAttr:   {DataType: aws.String("String"), StringValue: aws.String(m.EventType)},
		},
		TopicArn: aws.String(p.topicArn),
	})
	if err != nil {
		return errors.Wrap(err, "Failed to publish event")
	}
	return nil
}

package sqssubscriber

import (
	"context"
	"time"

	"bitbucket.org/innius/go-pubsub/types"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

// SubscriberMessage is a struct to encapsulate subscriber messages and provide
// a mechanism for acknowledging messages _after_ they've been processed.
type SubscriberMessage interface {
	Message() (*types.RawEvent, error)
	Done(c context.Context) error
}

func (m *subscriberMessage) Done(c context.Context) error {
	_, err := m.sub.sqs.DeleteMessageWithContext(c, &sqs.DeleteMessageInput{
		QueueUrl:      m.sub.QueueUrl,
		ReceiptHandle: m.message.ReceiptHandle,
	})
	return err
}

func (m *subscriberMessage) Message() (*types.RawEvent, error) {
	b := []byte(aws.StringValue(m.message.Body))
	v := &types.RawEvent{}

	es := m.eventSourceFromMessageAttrs()
	if es == nil {
		return nil, errors.New("invalid event message: event does not have an event source")
	}
	v.EventSource = aws.StringValue(es)

	es = m.eventTypeFromMessageAttrs()
	if es == nil {
		return nil, errors.New("invalid event message: event does not have an event type")
	}
	v.EventType = aws.StringValue(es)

	v.RawMessage = b

	return v, nil
}

func (m *subscriberMessage) eventSourceFromMessageAttrs() *string {
	if v, ok := m.message.MessageAttributes[types.MessageSourceAttr]; ok {
		return v.StringValue
	}
	return nil
}

func (m *subscriberMessage) eventTypeFromMessageAttrs() *string {
	if v, ok := m.message.MessageAttributes[types.MessageTypeAttr]; ok {
		return v.StringValue
	}
	return nil
}

type subscriberMessage struct {
	sub     *sqsPollingClient
	message *sqs.Message
	stop    chan error
}

type sqsPollingClient struct {
	MaxNumberOfMessages *int64
	WaitTimeSeconds     *int64
	VisibilityTimeout   *int64
	QueueUrl            *string
	sqsErr              error
	sqs                 sqsiface.SQSAPI
}

func (s *sqsPollingClient) Start(c context.Context) <-chan SubscriberMessage {
	output := make(chan SubscriberMessage)

	go func(s *sqsPollingClient, output chan SubscriberMessage) {
		defer close(output)

		for {
			select {
			case <-c.Done():
				return
			default:
				resp, err := s.sqs.ReceiveMessageWithContext(c, &sqs.ReceiveMessageInput{
					MaxNumberOfMessages:   s.MaxNumberOfMessages,
					QueueUrl:              s.QueueUrl,
					WaitTimeSeconds:       s.WaitTimeSeconds,
					VisibilityTimeout:     s.VisibilityTimeout,
					MessageAttributeNames: []*string{aws.String("All")},
				})

				if err != nil {
					s.sqsErr = err
					return
				}

				if len(resp.Messages) == 0 {
					time.Sleep(1 * time.Second)
					continue
				}
				logrus.Debugf("received %d messages", len(resp.Messages))
				for _, msg := range resp.Messages {
					logrus.Debug(msg.String())
					output <- &subscriberMessage{
						sub:     s,
						message: msg,
					}
				}
			}
		}
	}(s, output)

	return output
}

func (s *sqsPollingClient) Error() error {
	return s.sqsErr
}

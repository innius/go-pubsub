package sqssubscriber

import (
	"bitbucket.org/innius/go-pubsub"
	machineevents "bitbucket.org/innius/go-pubsub/events/machines"
	"context"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/aws/aws-sdk-go/service/sqs"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
	"time"
)

type mySubscription struct {
	*machineevents.MachineEventHandler
	lastEvent *machineevents.MachineCreatedEvent
}

func TestSQSSubscriber(t *testing.T) {
	cfg := aws.NewConfig().
		WithDisableSSL(true).
		WithRegion(endpoints.UsEast1RegionID).
		WithEndpoint("http://localhost:9911")

	snsClient := sns.New(session.Must(session.NewSession(cfg)))
	pub := go_pubsub.NewPublisher(snsClient, "arn:aws:sns:us-east-1:1465414804035:test1", false)

	Convey("PubSub", t, func() {
		Convey("publish a message", func() {
			err := pub.Publish(context.Background(), machineevents.MachineCreatedEvent{ID: "foo", Name: "the foo machine"})
			So(err, ShouldBeNil)

			Convey("sqs subscription", func() {
				handler := &mySubscription{
					MachineEventHandler: &machineevents.MachineEventHandler{},
				}

				invocations := 0
				handler.OnMachineCreated(func(c context.Context, h machineevents.MachineCreatedEvent) error {
					invocations++
					handler.lastEvent = &h
					return nil
				})

				cfg := aws.NewConfig().WithRegion(endpoints.UsEast1RegionID).WithDisableSSL(true).WithEndpoint("http://localhost:9324")

				sub := NewSubscriber(sqs.New(session.Must(session.NewSession(cfg))), handler, Config{
					QueueUrl: "http://sqs:9324/queue/queue1",
				})

				ctx, _ := context.WithCancel(context.Background())
				go sub.Run(ctx)
				time.Sleep(100 * time.Millisecond)
				So(invocations, ShouldEqual, 1)
				So(handler.lastEvent.Name, ShouldNotBeEmpty)
				So(handler.lastEvent.ID, ShouldNotBeEmpty)
			})
		})
	})
}

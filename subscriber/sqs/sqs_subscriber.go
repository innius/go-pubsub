package sqssubscriber

import (
	"context"

	"bitbucket.org/innius/go-pubsub/types"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/sirupsen/logrus"
)

type Subscriber interface {
	Run(ctx context.Context) error
}

type Config struct {
	MaxNumberOfMessages int64
	WaitTimeSeconds     int64
	VisibilityTimeout   int64
	QueueUrl            string
}

const (
	defaultMaxNumberOfMessages = 10
	defaultWaitTimeSeconds     = 2
	defaultVisibilityTimeout   = 10
)

func (c Config) maxNumberOfMessages() *int64 {
	if c.MaxNumberOfMessages > 0 {
		return &c.MaxNumberOfMessages
	}
	return aws.Int64(defaultMaxNumberOfMessages)
}

func (c Config) waitTimeSeconds() *int64 {
	if c.WaitTimeSeconds > 0 {
		return &c.WaitTimeSeconds
	}
	return aws.Int64(defaultWaitTimeSeconds)
}

func (c Config) visibilityTimeout() *int64 {
	if c.VisibilityTimeout > 0 {
		return &c.VisibilityTimeout
	}
	return aws.Int64(defaultVisibilityTimeout)
}

func NewSubscriber(sqsapi sqsiface.SQSAPI, handler types.EventHandler, config Config) Subscriber {
	return &subscriber{
		sqsPollingClient: &sqsPollingClient{
			VisibilityTimeout:   config.visibilityTimeout(),
			WaitTimeSeconds:     config.waitTimeSeconds(),
			MaxNumberOfMessages: config.maxNumberOfMessages(),
			QueueUrl:            aws.String(config.QueueUrl),
			sqs:                 sqsapi,
		},
		handler: handler,
	}
}

type subscriber struct {
	*sqsPollingClient
	handler types.EventHandler
}

func (sub *subscriber) Run(ctx context.Context) error {
	stream := sub.Start(ctx)
	for e := range stream {
		msg, err := e.Message()
		if err != nil {
			logrus.Errorf("error while decoding sqs message; %+v", err)
		}
		if err := sub.handler.EventHandler(ctx, *msg); err != nil {
			logrus.Errorf("error while processing sqs message; %+v", err)
		}
		if err := e.Done(ctx); err != nil {
			logrus.Errorf("could not complete sqs message; %+v", err)
		}
	}

	return sub.Error()
}

package lambdasubscriber

import (
	"bitbucket.org/innius/go-pubsub/types"
	"context"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/sirupsen/logrus"
)

// NewSubscriber creates a new lambda event handler for SNS events;
// Deprecated: use NewSNSSubscriber instead
func NewSubscriber(handler types.EventHandler) func(ctx context.Context, event events.SNSEvent) error {
	return NewSNSSubscriber(handler)
}

// NewSNSSubscriber creates a new lambda event handler for SNS events;
func NewSNSSubscriber(handler types.EventHandler) func(ctx context.Context, event events.SNSEvent) error {
	return func(ctx context.Context, event events.SNSEvent) error {
		for _, r := range event.Records {
			entity := r.SNS
			event := types.RawEvent{
				RawMessage:  []byte(entity.Message),
				EventType:   eventType(entity),
				EventSource: eventSource(entity),
			}
			if err := handler.EventHandler(ctx, event); err != nil {
				logrus.Errorf("failed to process event %+v; %+v", event, err)
				return err
			}
		}
		return nil
	}
}

// NewSQSSubscriber creates a new lambda event handler for SQS events;
func NewSQSSubscriber(handler types.EventHandler) func(ctx context.Context, event events.SQSEvent) error {
	getSQSMessageAttribute := func(key string, attrs map[string]events.SQSMessageAttribute) string {
		if v, ok := attrs[key]; ok {
			return aws.StringValue(v.StringValue)
		}
		return ""
	}

	return func(ctx context.Context, event events.SQSEvent) error {
		for _, r := range event.Records {
			eventType := getSQSMessageAttribute(types.MessageTypeAttr, r.MessageAttributes)
			eventSource := getSQSMessageAttribute(types.MessageSourceAttr, r.MessageAttributes)
			event := types.RawEvent{
				RawMessage:  []byte(r.Body),
				EventType:   eventType,
				EventSource: eventSource,
			}
			if err := handler.EventHandler(ctx, event); err != nil {
				logrus.Errorf("failed to process event %+v; %+v", event, err)
				return err
			}
		}
		return nil
	}
}

func eventType(evt events.SNSEntity) string {
	return getMessageAttributeValue(types.MessageTypeAttr, evt.MessageAttributes)
}

func eventSource(evt events.SNSEntity) string {
	return getMessageAttributeValue(types.MessageSourceAttr, evt.MessageAttributes)
}

func getMessageAttributeValue(key string, attrs map[string]interface{}) string {
	if v, ok := attrs[key]; ok {
		vv := v.(map[string]interface{})
		return vv["Value"].(string)
	}
	return ""
}

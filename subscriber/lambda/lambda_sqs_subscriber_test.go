package lambdasubscriber

import (
	machineevents "bitbucket.org/innius/go-pubsub/events/machines"
	"bitbucket.org/innius/go-pubsub/types"
	"context"
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewSQSSubscriber(t *testing.T) {
	handler := &testSubscription{
		machines: &machineevents.MachineEventHandler{},
	}

	fired := false

	handler.machines.OnMachineCreated(func(c context.Context, h machineevents.MachineCreatedEvent) error {
		fired = true
		return nil
	})

	sut := NewSQSSubscriber(handler)

	testEvent := newSQSTestEvent(machineevents.MachineCreatedEvent{})

	err := sut(context.Background(), testEvent)

	assert.NoError(t, err)
	assert.True(t, fired)
}

func newSQSTestEvent(e types.EventMessage) events.SQSEvent {
	bits, err := json.Marshal(e)
	if err != nil {
		panic(err)
	}
	raw := types.RawEvent{
		EventType:   e.EventType(),
		EventSource: e.EventSource(),
		RawMessage:  bits,
	}
	bits, err = json.Marshal(raw)
	if err != nil {
		panic(err)
	}
	entity := events.SQSMessage{
		Body: string(bits),
		MessageAttributes: map[string]events.SQSMessageAttribute{
			types.MessageSourceAttr: {StringValue: aws.String(e.EventSource())},
			types.MessageTypeAttr:   {StringValue: aws.String(e.EventType())},
		},
	}

	return events.SQSEvent{
		Records: []events.SQSMessage{entity},
	}
}

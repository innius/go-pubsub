package lambdasubscriber

import (
	"context"
	"encoding/json"
	"testing"

	"bitbucket.org/innius/go-pubsub/events/companies"
	"bitbucket.org/innius/go-pubsub/events/machines"
	"bitbucket.org/innius/go-pubsub/types"
	"github.com/aws/aws-lambda-go/events"
	"github.com/stretchr/testify/assert"
)

type testSubscription struct {
	machines  *machineevents.MachineEventHandler
	companies *companyevents.CompanyEventHandler
}

func (ts *testSubscription) EventHandler(c context.Context, e types.RawEvent) error {
	return types.Handlers{ts.companies, ts.machines}.EventHandler(c, e)
}

func TestLambdaSubscriber(t *testing.T) {
	subscr := &testSubscription{
		machines:  &machineevents.MachineEventHandler{},
		companies: &companyevents.CompanyEventHandler{},
	}

	fired := false
	subscr.machines.OnMachineCreated(func(c context.Context, h machineevents.MachineCreatedEvent) error {
		fired = true
		return nil
	})

	f := NewSNSSubscriber(subscr)

	e := newTestEvent(machineevents.MachineCreatedEvent{})
	assert.NoError(t, f(context.Background(), e))
	assert.True(t, fired)
}

func newTestEvent(e types.EventMessage) events.SNSEvent {
	bits, err := json.Marshal(e)
	if err != nil {
		panic(err)
	}
	raw := types.RawEvent{
		EventType:   e.EventType(),
		EventSource: e.EventSource(),
		RawMessage:  bits,
	}
	bits, err = json.Marshal(raw)
	if err != nil {
		panic(err)
	}
	entity := events.SNSEntity{
		Message: string(bits),
		MessageAttributes: map[string]interface{}{
			types.MessageSourceAttr: map[string]interface{}{
				"Value": e.EventSource(),
			},
			types.MessageTypeAttr: map[string]interface{}{
				"Value": e.EventType(),
			},
		},
	}

	return events.SNSEvent{
		Records: []events.SNSEventRecord{
			{SNS: entity},
		},
	}
}

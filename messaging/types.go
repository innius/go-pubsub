package messaging

type MessageStatus uint8

const (
	NormalStatus MessageStatus = iota
	CautionaryStatus
	EmergencyStatus
)
